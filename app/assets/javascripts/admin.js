// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require jquery-ui
//= require libs/nestedSortable/jquery.mjs.nestedSortable
//= require libs/jquery.mobile.custom.min
//= require bootstrap-sprockets
//= require angular
//= require jquery_nested_form
//= require moment
//= require moment/cs.js
//= require tinymce


//= require admin/widgets
//= require admin/menu
//= require admin/reservations
//= require admin/settings/restaurant_scheme
//= require admin/settings/opening

//= require admin/AngularStartup


//= require admin/angular-services/TimePickerService


//= require admin/angular-controllers/TimePickerCtrl
//= require admin/angular-controllers/OpeningCtrl
//= require admin/angular-controllers/ReservationsSetCtrl
//= require admin/angular-controllers/ReservationsDisplayCtrl
//= require admin/angular-controllers/TagsCtrl

