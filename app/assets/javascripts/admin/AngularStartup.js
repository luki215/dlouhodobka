var myApp = angular.module("restaurantApp", []);

myApp.run(function($compile, $rootScope, $document) {
  return $document.on('page:load', function() {
    var body, compiled;
    body = angular.element('body');
    compiled = $compile(body.html())($rootScope);
    return body.html(compiled);
  });
});

$(document).on('ready page:load', function(){
angular.bootstrap("body", ['restaurantApp']);

  
});