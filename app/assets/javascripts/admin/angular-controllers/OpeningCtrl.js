


myApp.controller('OpeningCtrl', ['$scope', 'TimePickerService', function($scope,  TimePickerService) {
	//console.log(TimePickerService);
		$scope.timePicker = TimePickerService.timePickers;

		$scope.connecterPickers = TimePickerService.connectedFromTo;
		//console.log($scope.connecterPickers);
		
		//console.log($scope.timePicker);	

		$scope.connectFromTo = function(from,to){
			TimePickerService.insertConnectedFromTo(from, to);
		}

		$scope.$watch("timePicker", function(){
			//console.log(TimePickerService.connectedFromTo);
			$.each(TimePickerService.connectedFromTo, function(itemFrom, itemTo){
				var limitFrom = TimePickerService.timePickers[itemFrom];
				var firstH = limitFrom.h;
				var firstM = limitFrom.m;

				var limitTo = TimePickerService.timeOptions[itemFrom];				
				var lastH = limitTo.h[limitTo.h.length-1];
				var lastM = limitTo.m[lastH][limitTo.m[lastH].length-1];

				limitFrom = firstH +":"+ firstM;
				limitTo =  lastH + ":" + lastM;
		
				TimePickerService.insertTimeOptions(itemTo, limitFrom, limitTo);



			});
			//console.log(id);
			//console.log(id2);
		}, true);

		var opening_panel_default_values={};

		$scope.set_panel_to_default = function(id){
			opening_panel_default_values[id] = {hour_to:{}, hour_from:{}};
			opening_panel_default_values[id]['date'] = $scope.restaurant_opening_exceptions[id]['date_when']; 

			opening_panel_default_values[id]['hour_to']['h'] = $scope.timePicker['restaurant_opening_exceptions['+id+'][opening_exception_hours][to]'].h; 
			opening_panel_default_values[id]['hour_to']['m'] = $scope.timePicker['restaurant_opening_exceptions['+id+'][opening_exception_hours][to]'].m; 
			opening_panel_default_values[id]['hour_from']['h'] = $scope.timePicker['restaurant_opening_exceptions['+id+'][opening_exception_hours][from]'].h; 
			opening_panel_default_values[id]['hour_from']['m'] = $scope.timePicker['restaurant_opening_exceptions['+id+'][opening_exception_hours][from]'].m; 
			console.log(opening_panel_default_values);
		}
		$scope.reset_panel_to_default = function(id){
			console.log(opening_panel_default_values);
			$scope.restaurant_opening_exceptions[id]['date_when'] = opening_panel_default_values[id]['date']; 			
			$scope.timePicker['restaurant_opening_exceptions['+id+'][opening_exception_hours][to]'] = opening_panel_default_values[id]['hour_to']; 
			$scope.timePicker['restaurant_opening_exceptions['+id+'][opening_exception_hours][from]'] = opening_panel_default_values[id]['hour_from']; 
		}
		$scope.print_nice_time = function(time){
			return time['h'] +':' + time['m'];
		}

}]);









