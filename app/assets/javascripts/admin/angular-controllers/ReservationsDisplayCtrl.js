
myApp.controller('ReservationsDisplayCtrl', ['$scope', 'filterFilter', '$http', '$interval', function($scope, filterFilter, $http, $interval) {

	$scope.errors = {
		
	};

	$scope.reservations = {};


/** handling times **/ 
	$scope.getReservations = function(){
		//$scope.errors['get_reservations'] = [];
		
		var req = $http.get("/admin/reservations/get_reservations");
		req.success(function(data){

			//console.log(data);
			$.each(data.reservations, function(k, room){
				$.each(room.reservations, function(k1, reservation){
					//console.log(reservation.time_from);
					reservation.time_from_now = moment(reservation.time_from).fromNow();		
					reservation.ended = (moment().diff(reservation.time_from)>0)? true : false;
					reservation.time_diff = -moment().diff(reservation.time_from);
					//console.log(reservation.time_from);
				})
			});
			$scope.reservations =  data.reservations;
			//console.log(data.reservations[0].reservation_object);
			
		});
		req.error(function(){
			$scope.errors['date_errors'].push("Nastala neočekávaná chyba, zkuste znovu obnovit stránku");
		});

		
	}

	$scope.getReservations();
	$interval(function(){
		$scope.getReservations();

	},1000*60);






}]);

myApp.filter('orderObjectBy', function() {
  return function(items, field, reverse) {
    var filtered = [];
    angular.forEach(items, function(item) {
      filtered.push(item);
    });
    filtered.sort(function (a, b) {
      return (a[field] > b[field] ? 1 : -1);
    });
    if(reverse) filtered.reverse();
    return filtered;
  };
});

myApp.filter('strLimit', ['$filter', function($filter) {
   return function(input, limit) {
      if (input.length <= limit) {
          return input;
      }

      return $filter('limitTo')(input, limit) + '...';
   };
}]);

myApp.filter('isEmpty', function () {
        var bar;
        return function (obj) {
            for (bar in obj) {
                if (obj.hasOwnProperty(bar)) {
                    return false;
                }
            }
            return true;
        };
    });