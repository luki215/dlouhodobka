
myApp.controller('ReservationsSetCtrl', ['$scope', 'TimePickerService', 'filterFilter', '$http', function($scope,TimePickerService, filterFilter, $http) {

	$scope.errors = {
		date_errors: [], 
		get_room_errors: [],
		item: {}
	};




/** handling times **/ 
	$scope.getTimesForDay = function(){
		$scope.errors['date_errors'] = [];
		var chosen_date = encodeURIComponent($scope.reservation['DateInput']);
		var picker_from_id = "reservation[from]";
		var pickerFrom = TimePickerService.timePickers[picker_from_id];
		var picker_to_id = "reservation[to]";
		var pickerTo = TimePickerService.timePickers[picker_to_id];
		//console.log(chosen_date);
		//console.log($http);

		var req = $http.get("/admin/reservations/get_available_times_for_day?date="+chosen_date);
		req.success(function(data){

			//console.log(data);
			if( data.times.closed == "on"){
				$scope.errors['date_errors'].push("Omluváme se, pro zadané datum je restaurace uzavřena.");
			}
			else{

				TimePickerService.insertTimeOptions(picker_to_id, print_nice_time(data.times.from), print_nice_time(data.times.to) )

				updated_data_times_to = angular.copy(data.times.to);
				/********************** HERE YOU SET DEFAULT LENGTH OF RESERVATION *******************/
				updated_data_times_to.h  = pad(parseInt(updated_data_times_to.h )-1, 2);
				

				TimePickerService.insertTimeOptions(picker_from_id, print_nice_time(data.times.from), print_nice_time(updated_data_times_to) )
				
			}	

		});
		req.error(function(){
			$scope.errors['date_errors'].push("Nastala neočekávaná chyba, zkuste znovu obnovit stránku");
		});

		if($scope.errors['date_errors'].length == 0){
			 $scope.getAvailableTables();
		}
	}

/* syncing from-to */
	$scope.timePicker = TimePickerService.timePickers;

	$scope.connecterPickers = TimePickerService.connectedFromTo;
		
	$scope.connectFromTo = function(from,to){
		TimePickerService.insertConnectedFromTo(from, to);

	}


	$scope.$watch("timePicker", function(){

		//change available values in "to" fields
		$.each(TimePickerService.connectedFromTo, function(itemFrom, itemTo){
			var limitFrom = TimePickerService.timePickers[itemFrom];
			var firstH = limitFrom.h;
			var firstM = limitFrom.m;

			var limitTo = TimePickerService.timeOptions[itemTo];				
			var lastH = limitTo.h[limitTo.h.length-1];
			var lastM = limitTo.m[lastH][limitTo.m[lastH].length-1];

			/********************** HERE YOU SET DEFAULT LENGTH OF RESERVATION *******************/

			firstH = pad(parseInt(firstH)+1, 2);

			limitFrom = firstH +":"+ firstM;
			limitTo =  lastH + ":" + lastM;
			TimePickerService.insertTimeOptions(itemTo, limitFrom, limitTo); 

		});


		$scope.getAvailableTables("0");


	}, true);




	/** handling rooms and table selects **/

	$scope.rooms_data;

	$scope.getAvailableTables = function(){

		var chosen_date = encodeURIComponent($scope.reservation['DateInput']);
		var picker_from_id = "reservation[from]";
		var pickerFrom = TimePickerService.timePickers[picker_from_id];
		var picker_to_id = "reservation[to]";
		var pickerTo = TimePickerService.timePickers[picker_to_id];

		if(pickerTo){
			//console.log(chosen_date);
			var req = $http.get("/admin/reservations/get_available_tables", {
				params:{
					date: chosen_date,
					from: pickerFrom,
					to: pickerTo
				}
			});
			req.success(function(data){
				//console.log(data);
				$scope.rooms_data = data.rooms;

				$scope.filterRooms(); 
			});
			req.error(function(){
				$scope.errors['get_room_errors'].push("Nastala neočekávaná chyba, zkuste znovu obnovit stránku");
			});
		}
	}


	$scope.available_rooms=[];
	$scope.available_tables=[];
	$scope.filterRooms = function(reservation_id){
		var rooms_data =  angular.copy($scope.rooms_data);
		



		$.each($scope.reservation.item, function(reservation_id, reservation_item){

			//console.log("Cyklus pro" + reservation_id);
			var rooms = $scope.available_rooms[reservation_id];
			var tables = $scope.available_tables[reservation_id];


			var persons = reservation_item['persons'];
			var no_smoking_room = reservation_item['no_smoking_room'];

			$scope.errors.item[reservation_id] = $scope.errors.item[reservation_id] || {}
			$scope.errors.item[reservation_id]['no_table_errors'] = [];

			if(persons%1 !== 0 ){
				$scope.errors['no_table_errors'].push("Neplatný počet osob.");
			}

			filter_rooms = {};

			if(no_smoking_room)
				filter_rooms.smoking = false;
			else
				filter_rooms.smoking = true;

			rooms = filterFilter(rooms_data, filter_rooms);

			$scope.available_rooms[reservation_id] = rooms;
			

			$scope.reservation.item[reservation_id]['room'] =  $scope.reservation.item[reservation_id]['room'] || rooms[0].id;
			$scope.reservation.item[reservation_id]['room'] = (filterFilter(rooms, {id: $scope.reservation.item[reservation_id]['room']}).length == 0 )? rooms[0].id : $scope.reservation.item[reservation_id]['room'];


			var chosen_room_id = $scope.reservation.item[reservation_id]['room'];
			//console.log( filterFilter(rooms, {id: chosen_room_id}));

			//select tables for chosen room
			tables = filterFilter(rooms, {id: chosen_room_id})[0].tables;

		

			//console.log(tables);
			$.each($scope.reservation.item, function(k,v){
				//console.log(k);
				//console.log(reservation_id);
				//console.log(v);
				//console.log("---------------");
				if(k<reservation_id)
				{	
					tables = filterFilter(tables, {id: '!'+v['table'] });

				}
				
			});


	while (filterFilter(tables, {persons: persons }).length == 0 && persons < 100){
				persons++;			
			}

			tables = filterFilter(tables, {persons: persons });

			if(tables.length == 0 )
				$scope.errors.item[reservation_id]['no_table_errors'].push("Omlouváme se, pro daný počet osob již nemáme v této místnosti místo. Zkuste jinou místnost, rozdělit osoby mezi více stolů nebo jiný čas.");

			else{
				$scope.available_tables[reservation_id] = tables;

				$scope.reservation.item[reservation_id]['table'] = parseInt($scope.reservation.item[reservation_id]['table']);

				$scope.reservation.item[reservation_id]['table'] =  $scope.reservation.item[reservation_id]['table'] || tables[0].id;
				
			
				$scope.reservation.item[reservation_id]['table'] = (filterFilter(tables, {id: $scope.reservation.item[reservation_id]['table']}, true).length == 0 )? tables[0].id : $scope.reservation.item[reservation_id]['table'];
			}


		});







	};




}]);

var print_nice_time = function(time){
			return time['h'] +':' + time['m'];
}
