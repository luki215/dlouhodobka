
myApp.controller('TagsCtrl', ['$scope','$http', function($scope, $http) {
    
  
    $scope.add_tag_to_artcl = function(tag){
        var index = $scope.available_tags.indexOf(tag);
        $scope.available_tags.splice(index, 1);
        $scope.article_tags.push(tag);
    };
    $scope.remove_tag_to_artcl = function(tag){
        var index = $scope.article_tags.indexOf(tag);
        $scope.article_tags.splice(index, 1);
        $scope.available_tags.push(tag);
    }

    $scope.init = function(article_id, authenticity_token){
        $scope.article_id = article_id;
        $scope.authenticity_token = authenticity_token;
        set_tags();
    }

    $scope.create_tag = function(tag_name){
        var req_new_article_tag = $http.post("/admin/article_tags/", {name: tag_name, authenticity_token: $scope.authenticity_token});
        req_new_article_tag.success(function(){
            $scope.new_tag_name = "";
            $scope.available_tags.push(tag_name);
        });
    }

    set_tags = function(){

        $scope.article_tags = [];
        var req_articles_tags = $http.get("/admin/articles/" + $scope.article_id + "/tags");
        req_articles_tags.success(function(data){
            $scope.article_tags = data;
            $scope.available_tags = [];
            
                    var req_avail_tags = $http.get("/admin/article_tags/");
                    req_avail_tags.success(function(data){
                        $scope.available_tags = data.filter(function(t){ return !$scope.article_tags.includes(t); });
                    });
        });

        

    }

}]);
