myApp.controller('TimePickerCtrl', ['$scope',  'TimePickerService', function($scope,  TimePickerService) {
	//console.log(TimePickerService);
	$scope.timeSelectVals = [];
	$scope.timePicker =  [];
	$scope.initTimeSelect = function(id, from, to){
		TimePickerService.insertTimeOptions(id,from,to );	


	}


	$scope.initTimePicker = function(id, h, m){
		TimePickerService.insertTimePicker(id,{h:  h, m: m} );
	}


		$scope.timePicker = TimePickerService.timePickers;



		$scope.timeSelectVals = TimePickerService.timeOptions;



	




	$scope.handleTimeSet = function(id){
		var timePicker = $scope.timePicker[id],
			timeVals = $scope.timeSelectVals[id],
			firstHour = timeVals.h[0],
			lastHour = timeVals.h[timeVals.h.length-1];

		//set first suitable  minute value for start hour ( you have time from 6:20 and in minutes you have 05 -> set minutes to 20)
		if( (timePicker.h == firstHour) && timePicker.m < timeVals.m[firstHour][0] )
			timePicker.m = timeVals.m[firstHour][0] ;

		//same as above but for last hour and last minute possible
		if( (timePicker.h == lastHour) && timePicker.m > timeVals.m[lastHour][timeVals.m[lastHour].length-1] )
			timePicker.m = timeVals.m[lastHour][timeVals.m[lastHour].length-1] ;
	}

	

}]);







