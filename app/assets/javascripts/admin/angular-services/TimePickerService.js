
myApp.factory('TimePickerService', function() {
	var timePickerSrv = {};

	timePickerSrv.timeOptions = {};
	timePickerSrv.timePickers = {};
	timePickerSrv.connectedFromTo = {};

	timePickerSrv.insertTimePicker = function(id, obj){
		timePickerSrv.timePickers[id] = obj;

	}
	timePickerSrv.insertConnectedFromTo = function(picker1, picker2){

		timePickerSrv.connectedFromTo[picker1] = picker2;
		//console.log(timePickerSrv.connectedFromTo);

	}

	timePickerSrv.insertTimeOptions = function(id, from, to){

			var hFrom = parseInt(from.split(":")[0]);
			var mFrom = parseInt(from.split(":")[1]);		
			var hTo = parseInt(to.split(":")[0]);
			var mTo = parseInt(to.split(":")[1]);

			//console.log(id, from, to);		

			timePickerSrv.timeOptions[id] = null;
			timePickerSrv.timeOptions[id] = {
				h: setHours(hFrom,hTo),
				m: []

			};			
			var hourLength = timePickerSrv.timeOptions[id].h.length-1;
			$.each(timePickerSrv.timeOptions[id].h, function(i, val){
				
				timePickerSrv.timeOptions[id].m[val] = setMinutes(0, 59);

				if(i == 0)
				{
					var wrong_minutes = setMinutes(0, mFrom);
					wrong_minutes.pop();
					timePickerSrv.timeOptions[id].m[val] = $(timePickerSrv.timeOptions[id].m[val]).not(wrong_minutes).get();					
				}
				if (i == hourLength) {	
					timePickerSrv.timeOptions[id].m[val] = $(timePickerSrv.timeOptions[id].m[val]).filter(setMinutes(0, mTo)).get();
				}
			});	


			setupTimePicker(id);

	}


	var setupTimePicker = function(picker_id){
		var allowed = timePickerSrv.timeOptions[picker_id];
		var actual = timePickerSrv.timePickers[picker_id];
		//console.log(picker_id);
		//console.log(timePickerSrv.timePickers);

				/* repairing time inputs */
				
				var firstH = allowed.h[0];
				var lastH = allowed.h[allowed.h.length-1];
				var firstM = allowed.m[firstH][0];
				var lastM = allowed.m[lastH][allowed.m[lastH].length-1];

			

				//console.log(pickerFrom.h, lastH);
				if( (parseInt(actual.h) <= parseInt(firstH)) ){
					actual.h = firstH;}
				if( (parseInt(actual.h) == parseInt(firstH)) && (parseInt(actual.m) <= parseInt(firstM)) )
					actual.m = firstM;
				if( (parseInt(actual.h) >= parseInt(lastH)) )
					actual.h = lastH;
				if( (parseInt(actual.h) == parseInt(lastH)) && (parseInt(actual.m) >= parseInt(lastM)) )
					actual.m = lastM;


	}







  	return timePickerSrv;
});


var setMinutes = function(from, to){
  		return range(from, to, 5);
  	}
var setHours = function(from, to){
  		return range(from, to, 1);
  	}
function range(start, stop, step){
  var a=[pad(start,2)], b=start;
  while(b<=stop-step){b+=step;a.push(pad(b, 2))}
  	return a;
};
function pad(num, size) {
    var s = "00" + num;
    return s.substr(s.length-size);
}

var getKeyByValue = function (object, value) {
	var ret_key = null;


	$.each(object, function(key,val){
		if (object[key] == value)
			ret_key = key;
	});

  	 return  ret_key; 
}


