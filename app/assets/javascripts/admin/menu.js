$(document).on('ready page:load', function(){


	right_nav = $('.menu .menu-right-sidebar .menu-right-nav');
  
/* reordering */
  var menu_nav_relocate = function(a,item){
        moved_item = item.item
        item_id = moved_item.attr('id').split("_")[1]
        serialized = right_nav.nestedSortable('toArray')
        hierarched = right_nav.nestedSortable('toHierarchy')
        console.log(serialized);
        var request = $.ajax({
                      url: "/admin/menu_categories/reorder",
                      type: "POST",
                      data: { items: serialized,
                              items_hierarchy: hierarched,
                              moved_item_id: item_id

                            },
                      dataType: "json"
                    });



  };
	right_nav.nestedSortable({
            forcePlaceholderSize: true,
            handle: 'div > span > span.icos-left > i',
            helper:     'clone',
            items: 'li',
            placeholder: 'placeholder',
            revert: 250,
            tabSize: 25,
            tolerance: 'pointer',
            toleranceElement: '> div',
            maxLevels: 6,
            isTree: true,
            expandOnHover: 700,
            startCollapsed: false,
            stop: function(a,item){menu_nav_relocate(a,item)}
  });


/* editing categories */


var items = right_nav.find("li");
  items.find('span.edit').hide();

  items.find('.ico.ico-edit').click(function(){switchCatToEdit(this);});
  items.find('.ico.ico-back').click(function(){switchCatToInfo(this);});

  var switchCatToEdit = function($this){
    $this = $($this).closest('li').children('div');
    $($this).children('span.info').hide();
    $($this).children('span.edit').show();
    $($this).find('.cat-name-edit-field').focus();

  }
  var switchCatToInfo = function($this){
    $this = $($this).closest('li').children('div');
    $($this).children('span.info').show();
    $($this).children('span.edit').hide();
    //console.log($($this).find("form"));
    $($this).find("form")[0].reset();
  }





/* reordering foods*/
  var foods = $('.menu .food-panels-container .food-list');


  var menu_food_relocate = function(a,item){
        moved_item = item.item
        item_id = moved_item.attr('id').split("_")[1]
        serialized = foods.nestedSortable('toArray')
        hierarched = foods.nestedSortable('toHierarchy')
        //console.log(serialized);
        var request = $.ajax({
                      url: "/admin/menu_categories/reorder_foods",
                      type: "POST",
                      data: { items: serialized,
                              items_hierarchy: hierarched,
                              moved_item_id: item_id

                            },
                      dataType: "json"
                    });



  };

  //console.log(foods);


  foods.nestedSortable({
            forcePlaceholderSize: true,
            handle: 'div .icos-left i',
            helper:  'clone',
            items: 'li',
            placeholder: 'placeholder',
            revert: 250,
            tabSize: 25,
            tolerance: 'pointer',
            toleranceElement: '> div',
            maxLevels: 2,
            isTree: false,
            expandOnHover: 700,
            startCollapsed: false,
            disableNestingClass: 'ui-nestedSortable-no-nesting',
            axis: "y",
            
            stop: function(a,item){menu_food_relocate(a,item)}
  });







});