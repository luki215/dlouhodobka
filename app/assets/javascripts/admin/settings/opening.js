
$(document).on('ready page:load', function(){

	$.datepicker.regional['cs'] = { 
                closeText: 'Cerrar', 
                prevText: 'Předchozí', 
                nextText: 'Další', 
                currentText: 'Hoy', 
                monthNames: ['Leden','Únor','Březen','Duben','Květen','Červen', 'Červenec','Srpen','Září','Říjen','Listopad','Prosinec'],
                monthNamesShort: ['Le','Ún','Bř','Du','Kv','Čn', 'Čc','Sr','Zá','Ří','Li','Pr'], 
                dayNames: ['Neděle','Pondělí','Úterý','Středa','Čtvrtek','Pátek','Sobota'], 
                dayNamesShort: ['Ne','Po','Út','St','Čt','Pá','So',], 
                dayNamesMin: ['Ne','Po','Út','St','Čt','Pá','So'], 
                weekHeader: 'Sm', 
                dateFormat: 'dd.mm.yy', 
                firstDay: 1, 
                isRTL: false, 
                showMonthAfterYear: false, 
                yearSuffix: ''}; 
 
$.datepicker.setDefaults($.datepicker.regional['cs']);


	$('.datepicker ').each(function(){
                $(this).datepicker( {minDate: 0});

        });





var opening_panels = $('.opening-panels-container');

var items = opening_panels.find(".opening-panel");


  var origin_form = {};

  var switchPanelToEdit = function(opening_panel){
      if(!opening_panel.find('.panel-collapse').is('.collapsing')){
        opening_panel.find('.panel-heading span.info').hide();
        opening_panel.find('.panel-heading span.edit').show();
      }

  }

  var switchPanelToInfo = function(opening_panel){
    if(!opening_panel.find('.panel-collapse').is('.collapsing')){
      opening_panel.find('span.info').show();
      opening_panel.find('span.edit').hide();
    }
   
  }

  //console.log(items)  ;
  items.find('span.edit').hide();

  items.on("click", '.panel-heading .ico.ico-edit, .panel-heading .ico.ico-new', function(){ switchPanelToEdit( $(this).closest('.opening-panel')) });
  items.on("click", '.panel-heading .ico.ico-back', function(){switchPanelToInfo( $(this).closest('.opening-panel')) });
  
  switchPanelToEdit($('.opening-panel .panel-collapse.in').closest('.opening-panel')); 






});