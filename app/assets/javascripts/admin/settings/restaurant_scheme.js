
/* rooms switching between edit and show */

$(document).on('ready page:load', function(){


	var rooms = $('.room-panels-container');

	var items = rooms.find(".room");
	//console.log(rooms)  ;




	/* new room - sync title in panel with room name */
	
	$('.new-room').find('.room-table-number').val("1");
	$('#new_room_form_name').on("input" , function(e){
	 if($(this).data("lastval")!= $(this).val()){
	     $(this).data("lastval",$(this).val());
	        if($(this).val()==""){
	          $(this).closest('.room').children('.panel-heading').find('.panel-heading-content').text("Nová místnost");
	        }
	        else{
	             $(this).closest('.room').children('.panel-heading').find('.panel-heading-content').text($(this).val());
	        }
	  }
	});    

	
	/* adding new table - automatic ID */

	$(document).on('nested:fieldAdded', function(event){
		  // this field was just inserted into your form
		  
		  if(event.field.prev()){
			var prev_table_no = parseInt( event.field.prevAll(':visible').first().find('.room-table-number').val() );
			event.field.find('.room-table-number').val(prev_table_no+1);

		  }
		  

	});



	/* adding new table - handling tab */
	rooms.find('.room-tables-list').on('keydown', '.room-table:visible:last .room-table-last-input', function(e) { 
	  var keyCode = e.keyCode || e.which; 

	  if (keyCode == 9 && !e.shiftKey) { 
	    $(this).closest('.room').find('.add_nested_fields').click();
	  } 
	});

	/** tables edit */
	items.find('.room-tables-list').on('click', '.room-table .ico.ico-edit', function(){
		$(this).closest('.room-table').find('.room-table-number').focus();
		return false;
	})



});