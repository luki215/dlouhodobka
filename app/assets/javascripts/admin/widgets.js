$(document).on('ready page:load', function(){
/* main admin nav */

$('#nav-switch').click(function(){
  $('#page-wrapper').toggleClass('hidden-nav');

  return false;
});

$(window).on("swipeleft", function(){
  if(!$('#page-wrapper').is('.hidden-nav'))
    $('#page-wrapper').addClass('hidden-nav');

});
$(window).on("swiperight", function(){  console.log("ssss");
  if($('#page-wrapper').is('.hidden-nav'))
    $('#page-wrapper').removeClass('hidden-nav');
});



// hide form buttons 
$('.image-upload-revert-image-btn, .image-upload-save-image-btn').hide(); 
/* image-upload-widget */
$('.image-upload-widget').on("change", '.image-upload-input', function(){
  var upload_container = $(this).closest('.image-upload-widget');

  //show form buttons
  upload_container.find('.image-upload-revert-image-btn, .image-upload-save-image-btn').show(); 
  //user has chosen image
  if($(this).context.value){
      //get image name
      var image_file = $(this).context.value.toString();
      image_file = image_file.split("\\");
      image_file = image_file[image_file.length-1];

      // change captions
      upload_container.find('.image-caption').text( image_file);
      upload_container.find('.image-upload-button-caption').text("Jiný obrázek");

      //don't delete existing attachment
      upload_container.find('.image-upload-delete-image-input').prop('checked', false);

      //hide food image if editing and display only form
      if(upload_container.children('.image-edit').length > 0 ){
          upload_container.find('.image-upload-info-container').css("display", "block");
           upload_container.find('.image-upload-uploaded-image').css("display", "none");
      }
  }
  //user haven't chosen image
  else{
    upload_container.find('.image-caption').text( "Žádný obrázek");
    $(this).prev().text("Nahrát obrázek");
        //delete existing attachment
    upload_container.find('.image-upload-delete-image-input').prop('checked', true);
  }
     upload_container.find('.image-edit').addClass('image-new').removeClass('image-edit');
});

$('.image-upload-widget').on("click",".image-upload-delete-image-btn", function(){
    var upload_container = $(this).closest('.image-upload-widget');


    //show form buttons
    upload_container.find('.image-upload-revert-image-btn, .image-upload-save-image-btn').show(); 

    //reset image input 
    var input = upload_container.find('.image-upload-input');
    input.wrap('<form>').closest('form').get(0).reset();
    input.unwrap();

    //delete existing attachment
   // console.log(upload_container.find('.image-upload-delete-image-input'));
    upload_container.find('.image-upload-delete-image-input').prop('checked', true);

    //change captions
     upload_container.find('.image-upload-button-caption').text("Nahrát obrázek");
     upload_container.find('.image-caption').text("Žádný obrázek");
    
    //hide food image if editing and display only form
      if(upload_container.children('.image-edit').length > 0 ){
          upload_container.find('.image-upload-info-container').css("display", "block");
           upload_container.find('.image-upload-uploaded-image').css("display", "none");
      }
      upload_container.find('.image-edit').addClass('image-new').removeClass('image-edit');

    return false;
});

$('.image-upload-widget').on("click",".image-upload-delete-image-btn", function(){
    var upload_container = $(this).closest('.image-upload-widget');


    //reset image input 
    var input = upload_container.find('.image-upload-input');
    input.wrap('<form>').closest('form').get(0).reset();
    input.unwrap();

    //delete existing attachment
    //console.log(upload_container.find('.image-upload-delete-image-input'));
    upload_container.find('.image-upload-delete-image-input').prop('checked', true);

    //change captions
     upload_container.find('.image-upload-button-caption').text("Nahrát obrázek");
     upload_container.find('.image-caption').text("Žádný obrázek");
    
    //hide food image if editing and display only form
      if(upload_container.children('.image-edit').length > 0 ){
          upload_container.find('.image-upload-info-container').css("display", "block");
           upload_container.find('.image-upload-uploaded-image').css("display", "none");
      }
      upload_container.find('.image-edit').addClass('image-new').removeClass('image-edit');

    return false;
});



// get widget with form buttons revert workin 
var form_content = [];
$('.image-upload-widget.with-form-buttons').each(function(i){

  $(this).attr("id", i);
  form_content[i] = $(this).html();

});

$('.image-upload-widget.with-form-buttons').on('click', '.image-upload-revert-image-btn', function(){
  var container = $(this).closest('.image-upload-widget');
  container.html(form_content[parseInt(container.attr("id"))])
});













/****** edit save and revert panel handle *****/

/* editing_panels switching between edit and show */


var editing_panels = $('.editing-panels-container');

var items = editing_panels.find(".editing-panel");
  var origin_form = {};

  var switchPanelToEdit = function(editing_panel){
      if(!editing_panel.find('.panel-collapse').is('.collapsing')){

        origin_form[editing_panel.attr("id")] = editing_panel.html();
        editing_panel.find('.panel-heading span.info').hide();
        editing_panel.find('.panel-heading span.edit').show();
      }

  }

  var switchPanelToInfo = function(editing_panel){
    if(!editing_panel.find('.panel-collapse').is('.collapsing')){
      editing_panel.find('span.info').show();
      editing_panel.find('span.edit').hide();
      editing_panel.find('.panel-collapse').on('hidden.bs.collapse', function(){
        editing_panel.html( origin_form[editing_panel.attr('id')] );
      });
    }
   
  }

  //console.log(items)  ;
  items.find('span.edit').hide();

  items.on("click", '.panel-heading .ico.ico-edit, .panel-heading .ico.ico-new', function(){ switchPanelToEdit( $(this).closest('.editing-panel')) });
  items.on("click", '.panel-heading .ico.ico-back', function(){switchPanelToInfo( $(this).closest('.editing-panel')) });
  
  switchPanelToEdit($('.editing-panel .panel-collapse.in').closest('.editing-panel')); 


});

