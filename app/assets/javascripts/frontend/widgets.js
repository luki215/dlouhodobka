jQuery(function(){

        $.datepicker.regional['cs'] = { 
                        closeText: 'Cerrar', 
                        prevText: 'Předchozí', 
                        nextText: 'Další', 
                        currentText: 'Hoy', 
                        monthNames: ['Leden','Únor','Březen','Duben','Květen','Červen', 'Červenec','Srpen','Září','Říjen','Listopad','Prosinec'],
                        monthNamesShort: ['Le','Ún','Bř','Du','Kv','Čn', 'Čc','Sr','Zá','Ří','Li','Pr'], 
                        dayNames: ['Neděle','Pondělí','Úterý','Středa','Čtvrtek','Pátek','Sobota'], 
                        dayNamesShort: ['Ne','Po','Út','St','Čt','Pá','So',], 
                        dayNamesMin: ['Ne','Po','Út','St','Čt','Pá','So'], 
                        weekHeader: 'Sm', 
                        dateFormat: 'dd.mm.yy', 
                        firstDay: 1, 
                        isRTL: false, 
                        showMonthAfterYear: false, 
                        yearSuffix: ''}; 
         
        $.datepicker.setDefaults($.datepicker.regional['cs']);


        	$('.datepicker ').each(function(){
                        $(this).datepicker( {minDate: 0});

                });



        $('.reservation-widget .send_status>.alert').hide();

        $('.reservation-form').on("submit", function(){
                var form = $(this);
                $.post($(this).attr("action"), $(this).serialize()).success(function(){
                     form.fadeOut(function(){
                        form.parent().find('.send_status>.success').show();
                     });
                }).error(function(){
                         form.parent().find('.send-status>.error').show();
            
                });
                return false;
        });

});