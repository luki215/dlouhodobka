class Admin::ArticleTagsController < ApplicationController
    #json-only controller

    def create
        tag_name = params["name"]
        tag = ArticleTag.create(name: tag_name)
        if tag.save
            render status: 201,  json: nil
        else
            render status: 400, json: tag.errors 
        end
    end
    def index
        render json: ArticleTag.all.map {|t| t.name}
    end
    
end
