module Admin
	class ArticlesController < Admin::BaseController
		
		before_action :set_article, only: [:edit, :destroy, :update, :tags]

	    # GET /articles
	    def index
	      @admin_articles = Article.all
	    end

	    # GET /articles/new

	    def new
	      @admin_article = Article.new
	    end

	    # GET /articles/1/edit
	    def edit
	    end

	    # POST /articles
	    def create
				@admin_article = Article.new(article_params)
				tags = get_and_create_tags(params[:article][:tags])
				@admin_article.article_tag.destroy_all
	        if @admin_article.save && (@admin_article.article_tag << tags unless tags.nil?)
	          redirect_to admin_articles_path, notice: 'Článek byl úspěšně vytvořen.' 
	        else
	          render :new 
	        end
	    end

	    # PATCH/PUT /articles/1
			def update
					tags = get_and_create_tags(params[:article][:tags])
					
					@admin_article.article_tag.destroy_all	
	        if @admin_article.update(article_params) && (@admin_article.article_tag << tags unless tags.nil?)
	          redirect_to admin_articles_path, notice: 'Článek byl úspěšně upraven.'
	        else
	           render :edit 
	        end
	    end

	    # DELETE /articles/1
	    def destroy
	      @admin_article.destroy
	        redirect_to admin_articles_path,  notice: 'Článek byl úspešně smazán.' 
	    end


			def tags 
				if @admin_article.nil?
					render status: 400, json: {errors: "No article with such article_id"}
				else
						render json: @admin_article.article_tag.all.map{ |tag| tag.name}
				end		
			end

	    private

				def set_article
					@admin_article = Article.friendly.find(params[:id])
					rescue 
						@admin_article = nil
	      end

	        # Never trust parameters from the scary internet, only allow the white list through.

	      def article_params
	        params.require(:article).permit(:title, :content)
	      end

				def get_and_create_tags(tags)
					tags.map{|tag| ArticleTag.find_or_create_by(name: tag)}	unless tags.nil?					
				end
	end
end