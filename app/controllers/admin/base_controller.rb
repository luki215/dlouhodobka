class Admin::BaseController < ApplicationController
	before_action :set_side_nav, :set_restaurant_settings, :check_authorization
	layout 'admin'	

	
	private
		def check_authorization
			unless is_admin?
				flash[:danger] ||= "Pro vstup do administrace je nutné přihlášení"
				redirect_to login_path
			end
		end

		def set_side_nav

			@side_nav =[
=begin				
				{
					'text' => 'Struktura webu',
					'controller' => 'navs',
					'action' => 'edit',
					'id'	=> 1,
					'ico' => 'fa fa-globe',
					
				},
=end
				{
					'text' => 'Statické stránky',
					'controller' => '/admin/static_pages',
					'action' => 'index',
					'ico' => 'fa fa-globe'
				},
				{
					'text' => 'Aktuality',
					'controller' => '/admin/articles',
					'action' => 'index',
					'ico' => 'fa fa-pencil-square-o',
					'subitems' => []
				},	
				{
					'text' => 'Jídelníček',
					'controller' => '/admin/menu_categories',
					'action' => (MenuCategory.all.empty?)? 'index': 'show'  ,
					'id' =>  (MenuCategory.all.empty?)? nil : MenuCategory.all.where(:ancestry=>nil).first.id, 					
					'ico' => 'fa fa-file-text-o',
					'subitems' => []
				},
				{
					'text' => 'Rezervace',
					'controller' => '/admin/reservations',
					'action' => 'index', 					
					'ico' => 'fa fa-calendar',
					'subitems' => []
				},
				{
					'text' => 'Restaurace',
					'controller' => '/admin/settings/opening',
					'action' => 'index',
					'id' =>  nil, 					
					'ico' => 'fa fa-cog',
					'subitems' => [

						{
							'text' => 'Otevírací doba',
							'controller' => '/admin/settings/opening',
							'action' => 'index',
							'id' =>  nil, 					
							'ico' => 'fa fa-clock-o',
							'subitems' => []
						},
						{
							'text' => 'Obecná nastavení',
							'controller' => '/admin/settings/general',
							'action' => 'index',
							'id' =>  nil, 					
							'ico' => 'fa fa-clock-o',
							'subitems' => []
						},
						{
							'text' => 'Struktura',
							'controller' => '/admin/settings/restaurant_scheme',
							'action' => 'index',
							'id' =>  nil, 					
							'ico' => 'fa fa-th',
							'subitems' => []
						}
					]
				}
=begin
				{
					'text' => 'Navigace',
					'controller' => 'navs',
					'action' => 'index',
					'ico' => 'fa fa-globe',
					
				},
=end
			]
		end	

end
