module Admin
	class MenuCategoriesController < Admin::BaseController

	  	before_action :set_category, only: [:edit, :destroy, :update, :show]
	   	def index
	      @foods = MenuFood.all
	      @food_categories = MenuCategory.all


	      @new_food_cat_form = MenuCategory.new


	   

	    end

	    def show
	    	@foods = MenuFood.where(menu_category_id: params[:id])

	      	@food_categories = MenuCategory.all
	      	
	      	@new_food_cat_form = MenuCategory.new

	      	@new_food_form = MenuFood.new(:menu_category_id => params[:id])

	    end

		def create
		    @category = MenuCategory.new(category_params)
	        if @category.save
	        	flash[:success] = "Kategorie v jídelníčku byla úspěšně vytvořena." 	        	 
	        else
	          	flash[:danger] = "Kategorie v jídelníčku nebyla vytvořena, zkuste to prosím znovu."
	        end
	        redirect_to admin_menu_category_path(@category.id)
	    end
    	def update
    		 @category.update(category_params)
			 if @category.save
	        	flash[:success] = "Kategorie v jídelníčku byla úspěšně změněna." 	        	 
	        else
	          	flash[:danger] = "Kategorie v jídelníčku nebyla změněna, zkuste to prosím znovu."
	        end
			 redirect_to admin_menu_category_path(@category.id)
	    end
		def destroy
			if @category.destroy
				flash[:success] = "Kategorie v jídelníčku byla úspěšně smazána." 
			else
				flash[:danger] = "Kategorie v jídelníčku nebyla smazána, zkuste to prosím znovu." 
			end
			redirect_to admin_menu_categories_path
	    end

	    def reorder
	    	items = params["items"]
	    	items_hierarchy = params["items_hierarchy"]
	    	moved_item = params['moved_item_id']



	    	
	    	item_obj = items.select{|k,v| v["item_id"] == moved_item}
	    	item_obj = item_obj.values.first
	    	item_obj_update = item_obj['parent_id'].blank?? nil : MenuCategory.find(item_obj['parent_id'])


	    	MenuCategory.find(item_obj['item_id']).update_attribute :parent, item_obj_update

	   
	    	MenuCategory.find(moved_item).insert_at(get_list_position(items_hierarchy, moved_item, 0))

	    	

   			render :nothing => true

	    end

	    def reorder_foods
	    	items = params["items"]
	    	items_hierarchy = params["items_hierarchy"]
	    	moved_item = params['moved_item_id']



	    	
	    	item_obj = items.select{|k,v| v["item_id"] == moved_item}
	    	item_obj = item_obj.values.first
	    	item_obj_update = item_obj['parent_id'].blank?? nil : MenuFood.find(item_obj['parent_id'])


	    	
	   
	    	MenuFood.find(moved_item).insert_at(get_list_position(items_hierarchy, moved_item, 0))

	    	

   			render :nothing => true

	    end




 		def get_list_position(hash, id, vnoreni) 
	    	hash_count = hash.count-1
	    	hash.each_with_index do |(key, val), i|
		    	hash_vals = val
		    	if hash_vals["id"] == id		    	 	
		    		return (key.to_i+1)
		    	elsif hash_vals["children"].blank?
					if(hash_count == i)
						return nil
					else						
						next
					end
		    	else
		    		unless (get_list_position(hash_vals["children"], id, vnoreni+1).nil?)  		
		    			return get_list_position(hash_vals["children"], id, vnoreni+1)
		    		else
		    			if(hash_count == i)
							return nil
						else
		    				next
		    			end
		    		end
		    	end
		    end		    
	    end

	   	private

		def set_category
	        @category = MenuCategory.friendly.find(params[:id])
	    end

	    def category_params
	    	params.require(:menu_category).permit(:name)
	    end
	end
end