module Admin
	class MenuFoodsController < Admin::BaseController
		before_action :set_food, only: [:destroy, :update]

  
	    # POST /menu_foods_foods
	    def create
	      @food = MenuFood.new(food_params)
	       
	        if @food.save
	        	flash[:success] = "Jídlo bylo úspěšně přidáno." 	        	 
	        else
	          	flash[:danger] = "Jídlo nebylo přidáno, zkuste to prosím znovu."
	        end
	        redirect_to admin_menu_category_path(food_params[:menu_category_id])


	    end

	    # PATCH/PUT /menu_foods_foods/1
	    def update
	        @food.update(food_params)
	       
	        if @food.save
	        	flash[:success] = "Jídlo bylo úspěšně změněno." 	        	 
	        else
	          	flash[:danger] = "Jídlo nebylo změněno, zkuste to prosím znovu."
	        end
	        redirect_to admin_menu_category_path(food_params[:menu_category_id])
	    end

	    # DELETE /menu_foods_foods/1
	    def destroy

	        
	        if @food.destroy
	        	flash[:success] = "Jídlo bylo úspěšně smazáno." 	        	 
	        else
	          	flash[:danger] = "Jídlo nebylo smazáno, zkuste to prosím znovu."
	        end
	        redirect_to admin_menu_category_path(params[:menu_category_id])
	    end


	    private

	    def set_food
	    	@food = MenuFood.friendly.find(params[:id])
	   	end

	    # Never trust parameters from the scary internet, only allow the white list through.

	    def food_params
	    	params.require(:menu_food).permit(:title, :name, :price, :quantity, :description, :image, :delete_image, :menu_category_id )
	    end



	end
end