module Admin
	class NavItemsController < Admin::BaseController
		include ApplicationHelper


		 before_action :set_nav, only: [:edit, :destroy, :show, :update]

	    # GET /articles
	    def index
	      @navs = NavItem.all
	    end

	    # GET /articles/1
	    def show    
	    end

	    # GET /articles/new

	    def new
	      @nav_item = NavItem.new(:parent_id => params[:parent_id])
	    end

	    # GET /articles/1/edit
	    def edit 
	    	redirect_to :controller => @nav_item_content_type[:admin_edit_controller], :action => @nav_item_content_type[:admin_edit_action], :id => @nav_item[:url_params][:id], :params => {:from_nav => true, :nav_id => params[:nav_id]}
	    end

	    # POST /articles
	    def create
	      nav_item_content_type = getNavItem(nav_params[:commit])
	      @nav_item = NavItem.new(item_type: nav_item_content_type[:id], nav_id: nav_params[:nav_id], :parent_id => nav_params[:parent_id])
	        if @nav_item.save
	          redirect_to :controller => nav_item_content_type[:admin_create_controller], :action => nav_item_content_type[:admin_create_action], :params => {:nav_item_id => @nav_item[:id], :from_nav => true, :nav_id => nav_params[:nav_id]}
	        else
	          render :new 
	        end
	    end

	    # PATCH/PUT /navs/1
	    def update
	        if @nav.update(nav_params)
	          redirect_to :controller => "navs", :action=>"edit", :id => nav_params[:nav_id], notice: 'Položka byla úspěšně změněna.'
	        else
	           render :edit 
	        end
	    end

	    # DELETE /articles/1
	    def destroy
	    	nav_item = @nav_item
	    	

	    	#application helper
	      destroy_nav_item(nav_item)
	        redirect_to :controller => "navs", :action=>"edit", :id => params[:nav_id], notice: 'Položka byla úspěšně smazána.'
	    end

	    private

	    	def set_nav
	        	@nav_item = NavItem.find_by id: params[:id]
	        	@nav_item_content_type = getNavItem @nav_item[:item_type]
	      	end

	        # Never trust parameters from the scary internet, only allow the white list through.

	      	def nav_params
	      		nav_params = params.permit(:commit, :nav_id)
	      		nav_params.merge! params.require(:nav_item).permit(:parent_id)


	      		nav_params
	      	end
	end

end
