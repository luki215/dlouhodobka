module Admin
	class NavsController < BaseController
	
		 before_action :set_nav, only: [:edit, :destroy, :show, :update]

	    # GET /articles
	    def index
	      @navs = Nav.all
	    end

	    # GET /articles/1
	    def show    
	    end

	    # GET /articles/new

	    def new
	      @nav = Nav.new
	    end

	    # GET /articles/1/edit
	    def edit
	    	@nav_items = NavItem.where("nav_id = ?", params[:id])
	    end

	    # POST /articles
	    def create
	      @nav = Nav.new(nav_params)
	        if @nav.save
	          redirect_to admin_navs_path, notice: 'Menu bylo úspěšně přidáno.' 
	        else
	          render :new 
	        end
	    end

	    # PATCH/PUT /navs/1
	    def update
	        if @nav.update(nav_params)
	          redirect_to admin_navs_path, notice: 'Menu bylo úspěšně změněno.'
	        else
	           render :edit 
	        end
	    end

	    # DELETE /articles/1
	    def destroy
	      @nav.destroy
	        redirect_to admin_navs_path,  notice: 'Menu bylo úspešně smazáno.' 
	    end

	    private

	    	def set_nav
	        	@nav = Nav.find(params[:id])

	      	end

	        # Never trust parameters from the scary internet, only allow the white list through.

	      	def nav_params
	        	params.require(:nav).permit(:id, :name)
	      	end
	end

end
