module Admin::Settings
	class OpeningController < Admin::BaseController
		def index
			@opening_exceptions = RestaurantOpeningExceptions.where("date_when >= ?", Date.today)
			@new_opening_exception = RestaurantOpeningExceptions.new

		end
		def update
			opening_params =  {restaurant_opening_exceptions: {date_when: params[:restaurant_opening_exceptions][:date_when], opening: params[:restaurant_opening_exceptions][params[:id]][:opening_exception_hours]  }}
			@opening_exception = RestaurantOpeningExceptions.find(params[:id]).update(opening_params[:restaurant_opening_exceptions])
	        if @opening_exception
	        	flash[:success] = "Výjimka v otvírací době byla úspěšně aktualizována." 	        	 
	        else
	          	flash[:danger] = "Výjimka v otvírací době nebyla aktualizována, zkuste to prosím znovu."
	        end
			redirect_to admin_settings_opening_index_path
			
		end
		def create
			opening_params =  {restaurant_opening_exceptions: { date_when: params[:restaurant_opening_exceptions][:date_when], opening: params[:restaurant_opening_exceptions][:opening_exception_hours]  }}
			@opening_exception = RestaurantOpeningExceptions.new(opening_params[:restaurant_opening_exceptions])
	        if @opening_exception.save
	        	flash[:success] = "Výjimka v otvírací době byla úspěšně vytvořena." 	        	 
	        else
	          	flash[:danger] = "Výjimka v otvírací době nebyla vytvořena, zkuste to prosím znovu."
	        end
			redirect_to admin_settings_opening_index_path

		end
		def destroy 
			@opening_exception = RestaurantOpeningExceptions.find(params[:id]).destroy()
	        if @opening_exception
	        	flash[:success] = "Výjimka v otvírací době byla úspěšně smazána." 	        	 
	        else
	          	flash[:danger] = "Výjimka v otvírací době nebyla smazána, zkuste to prosím znovu."
	        end
	        
			redirect_to admin_settings_opening_index_path
		end
	end
end