module Admin::Settings
	class RestaurantSchemeController < Admin::BaseController
		before_action :set_room, only: [:edit, :destroy, :update, :show]
		
		def index
			@tables = Table.all
			@rooms = Room.all	
		
			@new_room = Room.new
			@new_room.tables.build

		end

		def create

			@room = Room.new(room_params)
	        if @room.save
	        	flash[:success] = "Místnost byla úspěšně vytvořena." 	        	 
	        else
	          	flash[:danger] = "Místnost nebyla vytvořena, zkuste to prosím znovu."
	        end
	        redirect_to admin_settings_restaurant_scheme_index_path

		end

		def update

			@room.update(room_params)
	        if @room.save
	        	flash[:success] = "Místnost byla úspěšně změněna." 	        	 
	        else
	          	flash[:danger] = "Místnost nebyla vytvořena, zkuste to prosím znovu."
	        end
	        redirect_to admin_settings_restaurant_scheme_index_path
		end
		def destroy
			if @room.destroy
				flash[:success] = "Místnost byla úspěšně smazána." 
			else
				flash[:danger] = "Místnost nebyla smazána, zkuste to prosím znovu." 
			end
			redirect_to admin_settings_restaurant_scheme_index_path
		end


		private


		def room_params
	    	params.require(:room).permit(:name, :smoking,  tables_attributes: [:id, :number, :persons, :_destroy])
	    end
		
		def set_room
	        @room = Room.find(params[:id])
	    end
	end
end
