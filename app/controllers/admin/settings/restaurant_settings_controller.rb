module Admin::Settings
	class RestaurantSettingsController < Admin::BaseController
		
		def update
			a = settings_params

			a.merge!({opening_hours: params[:restaurant_settings][:opening_hours]}) unless params[:restaurant_settings][:opening_hours].nil?
			a.merge!({general_settings: params[:restaurant_settings][:general_settings]}) unless params[:restaurant_settings][:general_settings].nil?

			@restaurant_settings.update(a)
	        if @restaurant_settings.save
	        	flash[:success] = "Nastavení bylo úspěšně změněno." 	        	 
	        else
	          	flash[:danger] = "Nastavení nebyla změněno, zkuste to prosím znovu."
	        end

	        redirect_to request.referrer
		end

		private


		def settings_params
	    	params.require(:restaurant_settings).permit(:restaurant_scheme, :delete_image, :opening_hours)
	    end
		
	
	end
end