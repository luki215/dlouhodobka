module Admin
  class StaticPagesController < Admin::BaseController
    before_action :set_static_page, only: [:edit, :destroy, :update]

    # GET /static_pages
    def index
      @admin_static_pages = StaticPage.all
    end

    # GET /static_pages/new

    def new
      @admin_static_page = StaticPage.new()
    end

    # GET /static_pages/1/edit
    def edit
    end

    # POST /static_pages
    def create

      @admin_static_page = StaticPage.new(static_page_params)
      if @admin_static_page.save
            redirect_to admin_static_pages_path, notice: 'Stránka byla úspěšně vytvořena.' 
        else
          render :new 
        end
    end

    # PATCH/PUT /static_pages/1
    def update
        if @admin_static_page.update(static_page_params)
           if @from_nav
            redirect_to :controller => "navs", :action=>"edit", :id => params[:nav_id]
          else
            redirect_to admin_static_pages_path, notice: 'Stránka byla úspěšně upravena.' 
          end
        else
           render :edit 
        end
    end

    # DELETE /static_pages/1
    def destroy
      @admin_static_page.destroy
      redirect_to admin_static_pages_path, notice: 'Stránka byla úspěšně smazána.' 

    end

    private

      def set_static_page
        @admin_static_page = StaticPage.friendly.find(params[:id])
      end

        # Never trust parameters from the scary internet, only allow the white list through.

      def static_page_params
        params.require(:static_page).permit(:title,  :content).merge ({settings: params[:static_page][:settings]})
  
      end

  end
end