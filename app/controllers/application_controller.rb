class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.


  def set_restaurant_settings
    if RestaurantSettings.find_by(id: 1).nil?
      RestaurantSettings.new(id: 1).save        
    end
    @restaurant_settings = RestaurantSettings.find(1)

    @restaurant_settings
  end
  
  protected

  	def is_admin?
  		if(!session[:password].blank? && !session[:username].blank? && !(session[:password] == 'heslo') && !(session[:username] == 'restaurace'))
  			flash[:danger] = "Špatná kombinace jména a hesla"
  		end
  		(session[:password] == 'heslo') && (session[:username] == 'restaurace')
  	end

    
  protect_from_forgery with: :exception
end
