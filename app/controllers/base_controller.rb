class BaseController < ApplicationController
	layout 'frontend'
	before_action :set_nav, :set_restaurant_settings, only: [:index, :new, :show, :edit]
	

		
	protected


	def set_nav
		pages = StaticPage.all
		@nav =[
			{
				'text' => pages[0].title,
				'controller' => '/static_pages',
				'action' => 'index'
			}
		]
		pages.as_json.drop(1).each do |page|
			@nav += [{
				'text' => page["title"],
				'controller' => '/static_pages',
				'action' => 'show',
				'id' => page["slug"]
				}]

		end
		
	end


end
