class LoginController < BaseController
	layout 'login'	
	def new
		redirect_to admin_root_path if is_admin?


	end
	def create
		session[:username] = params[:username]
		session[:password] = params[:password]
		flash[:success] = "Přihlášení bylo úspěšné"
		redirect_to admin_root_path
	end

	def destroy
		reset_session
		flash[:success] = "Odhlášení bylo úspěšné"
		redirect_to login_path
	end



end
