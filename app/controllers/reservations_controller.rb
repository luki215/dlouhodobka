class ReservationsController < BaseController
	before_action :set_restaurant_settings
	def create
			
			reservation_params = params[:reservation]

			saved_succ = true

			reservation_params = {
				name: reservation_params[:name],
				email: reservation_params[:email],
				tel: reservation_params[:tel],
				note: reservation_params[:note],
				persons: reservation_params[:persons],
				time_from: Time.parse(reservation_params[:reservation_date] +" "+ printNiceTime(reservation_params[:from])),
				time_to: Time.parse(reservation_params[:reservation_date] +" "+ printNiceTime(reservation_params[:to]))
			}
			params[:reservation][:item].each do |i, item|
				reservation_params.merge!(table_id: item[:table].to_i)
				unless Reservation.new(reservation_params).save
					saved_succ = false
				end

			end
	       
	        if !saved_succ    	          	
	        	render status: :bad_request
	        end
	        render nothing: true




	    end



	    def get_available_times_for_day

	    	date = Date.strptime(params[:date], '%d.%m.%Y')

	    	exception_date = RestaurantOpeningExceptions.find_by(date_when: date)

	    	if exception_date.nil?
	    		opening_hours = @restaurant_settings.opening_hours[date.strftime("%a").downcase]
	    	else
	    		opening_hours = exception_date.opening

	    	end
    	
	    	if (date == Date.today() && opening_hours["from"]["h"].to_i < Time.now.hour ||  (opening_hours["from"]["h"].to_i == Time.now.hour && opening_hours["from"]["m"].to_i < Time.now.min) ) 
	    		opening_hours["from"]["h"] = (Time.now + 5*60).hour.to_s
	    		opening_hours["from"]["m"] = (Time.now+ 5*60).min.to_s	 
	    	
	    		if (Time.now.hour+1 > opening_hours["to"]["h"].to_i ||  (opening_hours["to"]["h"].to_i == Time.now.hour+1 && opening_hours["to"]["m"].to_i < Time.now.min)  )
		    		opening_hours["closed"] = "on"
		    	end

	    	end
	    	

	    	@times = {times: opening_hours}
					    	
	    	
	    	render json: @times

	    end

	    def get_available_tables

			rooms = Room.all.as_json
			tables = Table.all

			from = Time.parse(params[:date] +" "+ printNiceTime(JSON.parse(params[:from]).deep_symbolize_keys))
			to = Time.parse(params[:date] +" "+ printNiceTime(JSON.parse(params[:to]).deep_symbolize_keys))

			reserved = Reservation.all.where("(?>=time_from AND ?<=time_to) OR (?<=time_to AND ?>=time_from )", from, from, to, to ).select("table_id").as_json.map{|reservation| reservation["table_id"]  }
		

			rooms = rooms.map{|room| room.merge ({tables: tables.select{|a| (a[:room_id].to_i == room["id"]) && (!reserved.include? (a[:id]) )   }}) }




	    	render json: {rooms: rooms}
	    end



	    def get_reservations

			reservations = Reservation.all.where("time_from>=?", Date.current)
			rooms = Room.all.as_json


			reservation_return = {}

			rooms.each{|room| reservation_return[room["id"]] =  {:id=> room["id"], :name=> room["name"], :reservations => {} }  }


			reservations.each do |reservation|				
				reservation_write = reservation.as_json
				reservation_write.delete("updated_at")
				reservation_write.delete("created_at")
				reservation_write.merge! ({table_number: reservation.table.number})

				reservation_return[reservation.table.room_id.to_i][:reservations][reservation.id] = reservation_write
			end

	    	render json: {reservations: reservation_return}
	    end

	

	    private
	    	def printNiceTime (time)
				return time[:h]+":"+time[:m] + ":00"
			end


end
