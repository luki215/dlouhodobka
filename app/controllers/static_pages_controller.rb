class StaticPagesController < BaseController
  before_action :set_static_page, only: [:show]

  def index
    @page = StaticPage.find(1)
    @carousel_foods = MenuFood.where(menu_category_id: @page.settings[:food_carousel_category])
    @carousel_title = @page.settings[:food_carousel_title]
    @news = Article.all.limit(5)

  end
  def show    

    redirect_to root_path if @page.id == 1 

  end


  private
    # Use callbacks to share common setup or constraints between actions.

    def set_static_page
      @page = StaticPage.friendly.find(params[:id])

      rescue ActiveRecord::RecordNotFound => e
        redirect_to root_path
    end
end
