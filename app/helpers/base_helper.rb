module BaseHelper
		#returns string from - to opening hours or false if closed
	def opening_hours_today

	    	exception_date = RestaurantOpeningExceptions.find_by(date_when: Date.today())

	    	if exception_date.nil?
	    		opening_hours = @restaurant_settings.opening_hours[Date.today().strftime("%a").downcase]
	    	else
	    		opening_hours = exception_date.opening
	    	end
	    
	    	return false if opening_hours["closed"] == "on" 

	    	opening_hours
	end
	def openig_hours_for(day)
			exception_date = RestaurantOpeningExceptions.find_by(date_when: day)

	    	if exception_date.nil?
	    		opening_hours = @restaurant_settings.opening_hours[day.strftime("%a").downcase]
	    	else
	    		opening_hours = exception_date.opening
	    	end
	    
	    	return false if opening_hours["closed"] == "on" 

	    	opening_hours
	end
end
