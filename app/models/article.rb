class Article < ActiveRecord::Base
	extend FriendlyId

	has_and_belongs_to_many :article_tag, -> { uniq }

	validates :title, presence: true
	validates :content, presence: true
	default_scope { order(:updated_at => :desc) }
	friendly_id :title, use: :slugged
	 

end
