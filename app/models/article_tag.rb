class ArticleTag < ActiveRecord::Base
	has_and_belongs_to_many :article, -> { uniq }
	validates :name, uniqueness: :true
end
