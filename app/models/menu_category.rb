class MenuCategory < ActiveRecord::Base	
	attr_accessor :slug
	extend FriendlyId
	validates :name, presence: true

	
	has_many :menu_food 


	default_scope { order(:position => :asc) }

	has_ancestry
	acts_as_list scope: [:ancestry]
 	friendly_id :name, use: :slugged

end
