class MenuFood < ActiveRecord::Base
	attr_accessor :slug, :delete_image
	extend FriendlyId

 	before_validation { image.clear if delete_image == '1' }

  	friendly_id :name, use: :slugged

	belongs_to :menu_category
	
	has_attached_file :image, 	:styles => { :medium => "300x300>", :thumb => "100x100>" }, 
								:url  => "/assets/products/:id/:style/:basename.:extension",
                  				:path => ":rails_root/public/assets/products/:id/:style/:basename.:extension",
 								:default_url => "/images/:style/missing.png"
  

  	validates_attachment_content_type :image, :content_type => /\Aimage\/.*\Z/
	validates :name, presence: true	
	validates :price, presence: true	


	default_scope { order('position ASC') }

	acts_as_list scope: :menu_category

    

 	

end
