class Nav < ActiveRecord::Base
	has_many :nav_items, dependent: :destroy
end
