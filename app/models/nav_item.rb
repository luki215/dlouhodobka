class NavItem < ActiveRecord::Base
	belongs_to :nav
	has_ancestry
	serialize :url_params
end
