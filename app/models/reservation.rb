

class ReservationValidator < ActiveModel::Validator	
  def validate(record)
  	reserved = Reservation.all.where("(?>=time_from AND ?<=time_to) OR (?<=time_to AND ?>=time_from )", record.time_from, record.time_from, record.time_to, record.time_to ).select("table_id").as_json.map{|reservation| reservation["table_id"]  }
     
    record.errors[:base] << "Tento stůl je již zarezervován" if reserved.include? record.table_id
  end
end

class Reservation < ActiveRecord::Base
	include ActiveModel::Validations
	belongs_to :table
	
	default_scope {order('time_from ASC')}
	

  	validates_with ReservationValidator



end
