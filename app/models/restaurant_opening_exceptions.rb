class RestaurantOpeningExceptions < ActiveRecord::Base
	serialize :opening

	default_scope { order('date_when ASC') }

end
