class RestaurantSettings < ActiveRecord::Base
	attr_accessor :delete_image
 	

 	serialize :opening_hours
 	serialize :general_settings
	
	before_validation { restaurant_scheme.clear if delete_image == '1' }
	has_attached_file :restaurant_scheme, 	:styles => { :medium => "300x300>", :thumb => "100x100>" }, 
								:url  => "/assets/products/:id/:style/:basename.:extension",
                  				:path => ":rails_root/public/assets/products/:id/:style/:basename.:extension",
 								:default_url => "/images/:style/missing.png"
  

  	validates_attachment_content_type :restaurant_scheme, :content_type => /\Aimage\/.*\Z/
end
