class Room < ActiveRecord::Base
	#extend FriendlyId

	#attr_accessor :slug

	validates :name,  presence: true	

	has_many :tables 
 	accepts_nested_attributes_for :tables, :allow_destroy => true, reject_if: lambda {|attributes| attributes['number'].blank? || attributes['persons'].blank?}

 	#friendly_id :name, use: :slugged
end
