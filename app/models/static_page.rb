class StaticPage < ActiveRecord::Base
	extend FriendlyId

 	friendly_id :title, use: :slugged

	serialize :settings
	
	validates :title, presence: true

 end
