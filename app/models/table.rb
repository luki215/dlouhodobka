class Table < ActiveRecord::Base
	belongs_to :room

	has_many :reservations

	validates :number, :persons, presence: true	
end
