Rails.application.routes.draw do
 
  root "static_pages#index"

  resources :articles, path:"aktuality", :only=>['show']

  resources :login, :only=>['create']
  get 'login', to: 'login#new'
  get 'logout', to: 'login#destroy'

  resources :reservations, only: ['create'] do
    get 'get_available_times_for_day', on: :collection
    get 'get_available_tables', on: :collection
    get 'get_reservations', on: :collection
  end

  namespace :admin do
    root 'app#index'
    resources :static_pages
    resources :article_tags, :only => [:index, :create]
    resources :articles do
      get 'tags', on: :member
    end
    resources :navs do
      resources :nav_items
    end
    resources :menu_categories do      
      resources :menu_foods, :only=>['create', 'update', 'destroy'] 
      post 'reorder', on: :collection
      post 'reorder_foods', on: :collection
    end
    resources :menu_foods, :only=>['create', 'update'] 
    resources :reservations do
      get 'get_available_times_for_day', on: :collection
      get 'get_available_tables', on: :collection
      get 'get_reservations', on: :collection
    end


    namespace :settings do      
      resources :restaurant_settings
      resources :restaurant_scheme
      resources :opening
      resources :general, :only => ['index']
    end

  end

  get '/:id' => "static_pages#show"

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
