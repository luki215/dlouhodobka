class AddIndexToStaticPages < ActiveRecord::Migration
  def change
       add_index :static_pages, :slug, unique: true
  end
end
