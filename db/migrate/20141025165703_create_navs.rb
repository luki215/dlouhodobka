class CreateNavs < ActiveRecord::Migration	
  def change
    create_table :navs do |t|
      t.integer :type
      t.integer :order

      t.timestamps
    end
  end
end
