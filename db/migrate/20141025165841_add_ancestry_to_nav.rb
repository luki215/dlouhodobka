class AddAncestryToNav < ActiveRecord::Migration
  def change
    add_column :navs, :ancestry, :string
    add_index :navs, :ancestry
  end
end
