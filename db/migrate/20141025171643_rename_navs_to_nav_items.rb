class RenameNavsToNavItems < ActiveRecord::Migration
  def change
  	rename_table :navs, :nav_items
  end
end
