class AddNavIdToNavItems < ActiveRecord::Migration
  def change
  	add_column :nav_items, :nav_id, :integer
  	add_index :nav_items, :nav_id 
  end
end
