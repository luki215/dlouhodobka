class RenameNavItemTypeToNavItemItemType < ActiveRecord::Migration
  def change
  	rename_column :nav_items, :type, :item_type
  end
end
