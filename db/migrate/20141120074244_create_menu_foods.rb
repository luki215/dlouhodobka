class CreateMenuFoods < ActiveRecord::Migration
  def change
    create_table :menu_foods do |t|
      t.references :menu_category, index: true
      t.string :name, null: false
      t.integer :price, null: false
      t.text :description
      t.references :image, index: true

      t.timestamps
    end
  end
end
