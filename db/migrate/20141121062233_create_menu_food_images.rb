class CreateMenuFoodImages < ActiveRecord::Migration
  def change
    create_table :menu_food_images do |t|
    	t.references :menu_food
    	t.string :path
		t.integer :order


      t.timestamps
    end
  end
end
