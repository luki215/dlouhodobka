class AddSlugsToFoodAndFoodCategory < ActiveRecord::Migration
  def change
  	add_column :menu_categories, :slug, :string
  	add_column :menu_foods, :slug, :string
  end
end
