class AddAncestryToMenuCategories < ActiveRecord::Migration
  def change
    add_column :menu_categories, :ancestry, :string
    add_index :menu_categories, :ancestry
  end
end
