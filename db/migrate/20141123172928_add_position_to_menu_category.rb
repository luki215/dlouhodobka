class AddPositionToMenuCategory < ActiveRecord::Migration
  def change
    add_column :menu_categories, :position, :integer
  end
end
