class AddPositionToMenuFood < ActiveRecord::Migration
  def change
    add_column :menu_foods, :position, :integer
  end
end
