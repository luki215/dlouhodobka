class AddAttachmentImageToMenuFoods < ActiveRecord::Migration
  def self.up
    change_table :menu_foods do |t|
      t.attachment :image
    end
  end

  def self.down
    remove_attachment :menu_foods, :image
  end
end
