class AddRoomIdToTables < ActiveRecord::Migration
  def change
  	add_column :tables, :room_id, :string
  	add_index :tables, :room_id

  end
end
