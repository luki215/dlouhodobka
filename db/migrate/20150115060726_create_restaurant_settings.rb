class CreateRestaurantSettings < ActiveRecord::Migration
  def change
    create_table :restaurant_settings do |t|
      t.string :name
	  t.attachment :restaurant_scheme
      t.timestamps
    end
  end
end
