class CreateReservations < ActiveRecord::Migration
  def change
    create_table :reservations do |t|
      t.timestamp :at
      t.string :name
      t.string :email
      t.integer :duration
      t.integer :status
      t.integer :reservation_object_id
      t.string  :reservation_object_type
      t.timestamps null: false
    end

    add_index :reservations, :reservation_object_id

  end
end
