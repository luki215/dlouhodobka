class AddOpeningHoursToRestaurantSettings < ActiveRecord::Migration
  def change
  	 add_column :restaurant_settings, :opening_hours, :text
  end
end
