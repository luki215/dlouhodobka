class CreateRestaurantOpeningExceptions < ActiveRecord::Migration
  def change
    create_table :restaurant_opening_exceptions do |t|
      t.date :date_when, index: true
      t.string :opening

      t.timestamps
    end
  end
end
