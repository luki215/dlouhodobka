class EditReservationsFromTo < ActiveRecord::Migration
  def change
  	change_column :reservations, :at, :date
   	rename_column :reservations, :at, :reservation_date
  	add_column :reservations, :time_from, :time
  	add_column :reservations, :time_to, :time

  	add_index :reservations, :reservation_date
  	add_index :reservations, :time_from
  	add_index :reservations, :time_to
  end
end
