class ReduceReservationColumns < ActiveRecord::Migration
  def change
  	remove_column :reservations, :duration
  	remove_column :reservations, :status
  	remove_column :reservations, :reservation_date

  end
end
