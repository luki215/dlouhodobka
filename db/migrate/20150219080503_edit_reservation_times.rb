class EditReservationTimes < ActiveRecord::Migration
  def change
  	change_column :reservations, :time_to, :datetime, null: false
  	change_column :reservations, :time_from, :datetime, null: false
  end


end
