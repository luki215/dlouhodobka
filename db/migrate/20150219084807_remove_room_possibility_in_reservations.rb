class RemoveRoomPossibilityInReservations < ActiveRecord::Migration
  def change
  		remove_column :reservations, :reservation_object_id, :integer
  		remove_column :reservations, :reservation_object_type, :string
  		
  		add_column :reservations, :table_id,  :integer, null: false
  end
end
