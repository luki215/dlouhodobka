class AddTelAndInfoToReservation < ActiveRecord::Migration
  def change
  	add_column :reservations, :tel, :string
  	add_column :reservations, :note, :text
  end
end
