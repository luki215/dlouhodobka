class AddIndexToSlugFromStaticPages < ActiveRecord::Migration
  def change
  	add_index :static_pages, :slug
  end
end
