class AddGeneralSettingsToRestaurantSettings < ActiveRecord::Migration
  def change
    add_column :restaurant_settings, :general_settings, :text
  end
end
