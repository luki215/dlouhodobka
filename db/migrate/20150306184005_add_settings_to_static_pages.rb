class AddSettingsToStaticPages < ActiveRecord::Migration
  def change
    add_column :static_pages, :settings, :text
  end
end
