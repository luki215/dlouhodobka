class ConnectTagsWithArticles < ActiveRecord::Migration
  def change 
    create_table :article_tags_articles, id: false do |t|
      t.belongs_to :article  , index: true
      t.belongs_to :article_tag, index: true
    end
  end
end
