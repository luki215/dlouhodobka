# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20171016065553) do

  create_table "article_tags", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "article_tags_articles", id: false, force: true do |t|
    t.integer "article_id"
    t.integer "article_tag_id"
  end

  add_index "article_tags_articles", ["article_id"], name: "index_article_tags_articles_on_article_id", using: :btree
  add_index "article_tags_articles", ["article_tag_id"], name: "index_article_tags_articles_on_article_tag_id", using: :btree

  create_table "articles", force: true do |t|
    t.string   "title"
    t.string   "slug"
    t.text     "content"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "articles", ["slug"], name: "index_articles_on_slug", unique: true, using: :btree
  add_index "articles", ["title", "slug"], name: "index_articles_on_title_and_slug", using: :btree

  create_table "friendly_id_slugs", force: true do |t|
    t.string   "slug",                      null: false
    t.integer  "sluggable_id",              null: false
    t.string   "sluggable_type", limit: 50
    t.string   "scope"
    t.datetime "created_at"
  end

  add_index "friendly_id_slugs", ["slug", "sluggable_type", "scope"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type_and_scope", unique: true, using: :btree
  add_index "friendly_id_slugs", ["slug", "sluggable_type"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type", using: :btree
  add_index "friendly_id_slugs", ["sluggable_id"], name: "index_friendly_id_slugs_on_sluggable_id", using: :btree
  add_index "friendly_id_slugs", ["sluggable_type"], name: "index_friendly_id_slugs_on_sluggable_type", using: :btree

  create_table "menu_categories", force: true do |t|
    t.string   "name",       null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "ancestry"
    t.integer  "position"
    t.string   "slug"
  end

  add_index "menu_categories", ["ancestry"], name: "index_menu_categories_on_ancestry", using: :btree

  create_table "menu_food_images", force: true do |t|
    t.integer  "menu_food_id"
    t.string   "path"
    t.integer  "order"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "menu_foods", force: true do |t|
    t.integer  "menu_category_id"
    t.string   "name",               null: false
    t.integer  "price",              null: false
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "position"
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.string   "slug"
    t.string   "quantity"
  end

  add_index "menu_foods", ["menu_category_id"], name: "index_menu_foods_on_menu_category_id", using: :btree

  create_table "nav_items", force: true do |t|
    t.integer  "item_type"
    t.integer  "order"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "ancestry"
    t.integer  "nav_id"
    t.text     "url_params"
    t.text     "name"
  end

  add_index "nav_items", ["ancestry"], name: "index_nav_items_on_ancestry", using: :btree
  add_index "nav_items", ["nav_id"], name: "index_nav_items_on_nav_id", using: :btree

  create_table "navs", force: true do |t|
    t.string "name"
  end

  create_table "reservations", force: true do |t|
    t.string   "name"
    t.string   "email"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "time_from",  null: false
    t.datetime "time_to",    null: false
    t.integer  "persons"
    t.integer  "table_id",   null: false
    t.string   "tel"
    t.text     "note"
  end

  add_index "reservations", ["time_from"], name: "index_reservations_on_time_from", using: :btree
  add_index "reservations", ["time_to"], name: "index_reservations_on_time_to", using: :btree

  create_table "restaurant_opening_exceptions", force: true do |t|
    t.date     "date_when"
    t.string   "opening"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "restaurant_settings", force: true do |t|
    t.string   "name"
    t.string   "restaurant_scheme_file_name"
    t.string   "restaurant_scheme_content_type"
    t.integer  "restaurant_scheme_file_size"
    t.datetime "restaurant_scheme_updated_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "opening_hours"
    t.text     "general_settings"
  end

  create_table "rooms", force: true do |t|
    t.string   "name"
    t.boolean  "smoking"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "static_pages", force: true do |t|
    t.string   "title"
    t.text     "content"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "slug"
    t.text     "settings"
  end

  add_index "static_pages", ["slug"], name: "index_static_pages_on_slug", using: :btree

  create_table "tables", force: true do |t|
    t.integer  "number"
    t.integer  "persons"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "room_id"
  end

  add_index "tables", ["room_id"], name: "index_tables_on_room_id", using: :btree

end
