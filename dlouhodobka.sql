-- MySQL dump 10.13  Distrib 5.5.41, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: dlouhodobka
-- ------------------------------------------------------
-- Server version	5.5.41-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `articles`
--

DROP TABLE IF EXISTS `articles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `articles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8_unicode_ci,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_articles_on_slug` (`slug`),
  KEY `index_articles_on_title_and_slug` (`title`,`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `articles`
--

LOCK TABLES `articles` WRITE;
/*!40000 ALTER TABLE `articles` DISABLE KEYS */;
INSERT INTO `articles` VALUES (2,'alešek','alesek','<p>asdf</p>','2015-02-24 15:39:56','2015-02-24 15:39:56'),(3,'Tradiční jaro ve stodole opět v cyklobaru!','tradicni-jaro-ve-stodole-opet-v-cyklobaru','<p>Jarn&iacute; dekorace a vazby, uk&aacute;zky ručn&iacute;ch prac&iacute;, zahradn&iacute; keramika a sazenice, v&yacute;robky z kukuřičn&eacute;ho &scaron;ust&iacute; a dr&aacute;tovan&eacute; v&yacute;robky s&nbsp;předv&aacute;děn&iacute;m star&eacute;ho řemesla, zdoben&iacute; kraslic.</p>\r\n<p>Pro mlsn&eacute; jaz&yacute;čky medovina, pern&iacute;čky, ochutn&aacute;vky a prodej s&yacute;rů, uzen&yacute;ch a zabijačkov&yacute;ch specialit a jin&yacute;ch dom&aacute;c&iacute;ch dobrot J</p>\r\n<p>Atmosf&eacute;ru vykouzl&iacute; housle a harmonika L&aacute;di a Milana♫</p>\r\n<p>&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PŘIJĎTE SI PRO JARN&Iacute; N&Aacute;LADU!</p>\r\n<p><strong><span style=\"font-size: 14pt;\">Akce se kon&aacute; v sobotu 5. dubna 2014 od 10 do 16 hod v are&aacute;lu Cyklobaru.</span></strong></p>','2015-03-07 11:41:26','2015-03-07 11:41:26');
/*!40000 ALTER TABLE `articles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `friendly_id_slugs`
--

DROP TABLE IF EXISTS `friendly_id_slugs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `friendly_id_slugs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sluggable_id` int(11) NOT NULL,
  `sluggable_type` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `scope` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_friendly_id_slugs_on_slug_and_sluggable_type_and_scope` (`slug`,`sluggable_type`,`scope`),
  KEY `index_friendly_id_slugs_on_sluggable_id` (`sluggable_id`),
  KEY `index_friendly_id_slugs_on_slug_and_sluggable_type` (`slug`,`sluggable_type`),
  KEY `index_friendly_id_slugs_on_sluggable_type` (`sluggable_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `friendly_id_slugs`
--

LOCK TABLES `friendly_id_slugs` WRITE;
/*!40000 ALTER TABLE `friendly_id_slugs` DISABLE KEYS */;
/*!40000 ALTER TABLE `friendly_id_slugs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menu_categories`
--

DROP TABLE IF EXISTS `menu_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menu_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `ancestry` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `position` int(11) DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_menu_categories_on_ancestry` (`ancestry`)
) ENGINE=InnoDB AUTO_INCREMENT=64 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menu_categories`
--

LOCK TABLES `menu_categories` WRITE;
/*!40000 ALTER TABLE `menu_categories` DISABLE KEYS */;
INSERT INTO `menu_categories` VALUES (53,'Jídla','2015-02-28 12:00:19','2015-02-28 12:00:19',NULL,1,NULL),(54,'Nápoje','2015-02-28 12:00:37','2015-02-28 12:00:37',NULL,2,NULL),(55,'Hlavní jídla','2015-02-28 12:00:50','2015-03-06 18:53:35','53',2,NULL),(56,'Něco malého','2015-02-28 12:01:22','2015-03-06 18:46:17','53',1,NULL),(57,'S masem','2015-02-28 12:01:29','2015-03-06 18:54:59','53/55',1,NULL),(58,'Bezmasá','2015-02-28 12:01:36','2015-03-06 18:54:06','53/55',2,NULL),(59,'Přílohy','2015-02-28 12:02:13','2015-02-28 12:02:18','53',3,NULL),(60,'Alkoholické','2015-02-28 12:02:29','2015-02-28 12:02:35','54',1,NULL),(61,'Nealkoholické','2015-02-28 12:02:39','2015-02-28 12:02:45','54',2,NULL),(62,'Piva','2015-02-28 12:02:51','2015-02-28 12:03:47','54/60',1,NULL),(63,'Pizzy','2015-03-06 19:09:30','2015-03-06 19:09:56','53',4,NULL);
/*!40000 ALTER TABLE `menu_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menu_food_images`
--

DROP TABLE IF EXISTS `menu_food_images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menu_food_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_food_id` int(11) DEFAULT NULL,
  `path` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menu_food_images`
--

LOCK TABLES `menu_food_images` WRITE;
/*!40000 ALTER TABLE `menu_food_images` DISABLE KEYS */;
/*!40000 ALTER TABLE `menu_food_images` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menu_foods`
--

DROP TABLE IF EXISTS `menu_foods`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menu_foods` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_category_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `price` int(11) NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `position` int(11) DEFAULT NULL,
  `image_file_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image_content_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image_file_size` int(11) DEFAULT NULL,
  `image_updated_at` datetime DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `quantity` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_menu_foods_on_menu_category_id` (`menu_category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menu_foods`
--

LOCK TABLES `menu_foods` WRITE;
/*!40000 ALTER TABLE `menu_foods` DISABLE KEYS */;
INSERT INTO `menu_foods` VALUES (42,56,'Pikantní utopenec s chlebem',39,'','2015-03-06 18:46:33','2015-03-06 18:46:33',2,NULL,NULL,NULL,NULL,NULL,NULL),(43,56,'Nakládaný pikantní camembert, chléb',45,'','2015-03-06 18:49:05','2015-03-08 14:40:18',1,NULL,NULL,NULL,NULL,NULL,'1ks'),(44,56,'Hovězí tatarák, topinky ',179,'','2015-03-06 18:49:38','2015-03-06 18:49:38',3,NULL,NULL,NULL,NULL,NULL,NULL),(45,56,'Cykloburger s hranolkama',139,'','2015-03-06 18:50:09','2015-03-06 18:50:09',4,NULL,NULL,NULL,NULL,NULL,NULL),(46,56,'Cyklobageta balená na cestu ',59,'','2015-03-06 18:50:25','2015-03-06 18:50:25',5,NULL,NULL,NULL,NULL,NULL,NULL),(47,56,'Bageta',59,'smažené kuřecí řízečky, dressing, salát','2015-03-06 18:51:02','2015-03-06 18:51:02',6,NULL,NULL,NULL,NULL,NULL,NULL),(48,56,'Pražené mandle ',33,'','2015-03-06 18:51:25','2015-03-06 18:51:25',7,NULL,NULL,NULL,NULL,NULL,NULL),(49,57,'Grilovaná vepřová panenka špikovaná anglickou slaninou',189,'','2015-03-06 19:05:12','2015-03-08 16:59:05',1,'veprova_panenka.jpg','image/jpeg',203058,'2015-03-06 19:05:10',NULL,'200g'),(50,63,'Margarita',99,'tomat, eidam, bazalka ','2015-03-06 19:12:30','2015-03-06 19:12:30',1,'Eq_it-na_pizza-margherita_sep2005_sml.jpg','image/jpeg',657369,'2015-03-06 19:12:29',NULL,NULL),(51,63,'asdf',150,'','2015-03-06 22:26:10','2015-03-06 22:26:10',2,'veprova_panenka.jpg','image/jpeg',203058,'2015-03-06 22:26:04',NULL,NULL),(52,63,'Nakládaný pikantní camembert, chléb',150,'Super jídlo','2015-03-08 17:52:06','2015-03-08 17:52:06',3,'funny_fish3.jpg','image/jpeg',23049,'2015-03-08 17:51:58',NULL,'300g');
/*!40000 ALTER TABLE `menu_foods` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `nav_items`
--

DROP TABLE IF EXISTS `nav_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nav_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_type` int(11) DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `ancestry` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nav_id` int(11) DEFAULT NULL,
  `url_params` text COLLATE utf8_unicode_ci,
  `name` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `index_nav_items_on_ancestry` (`ancestry`),
  KEY `index_nav_items_on_nav_id` (`nav_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nav_items`
--

LOCK TABLES `nav_items` WRITE;
/*!40000 ALTER TABLE `nav_items` DISABLE KEYS */;
INSERT INTO `nav_items` VALUES (3,0,NULL,'2014-11-19 09:06:56','2014-11-19 09:07:02',NULL,1,'---\n:id: Ahoj\n','Ahoj'),(4,0,NULL,'2014-11-19 09:07:14','2014-11-19 09:07:19','3',1,'---\n:id: fgh\n','fgh'),(5,0,NULL,'2014-11-19 09:07:25','2014-11-19 09:07:29','3',1,'---\n:id: cvbn\n','cvbn'),(6,0,NULL,'2014-11-19 09:07:46','2014-11-19 09:07:52',NULL,1,'---\n:id: Kontakt\n','Kontakt'),(7,0,NULL,'2014-11-19 09:07:57','2014-11-19 09:08:03',NULL,1,'---\n:id: Haha\n','Haha');
/*!40000 ALTER TABLE `nav_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `navs`
--

DROP TABLE IF EXISTS `navs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `navs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `navs`
--

LOCK TABLES `navs` WRITE;
/*!40000 ALTER TABLE `navs` DISABLE KEYS */;
INSERT INTO `navs` VALUES (1,'Hlavní');
/*!40000 ALTER TABLE `navs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reservations`
--

DROP TABLE IF EXISTS `reservations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reservations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `time_from` datetime NOT NULL,
  `time_to` datetime NOT NULL,
  `persons` int(11) DEFAULT NULL,
  `table_id` int(11) NOT NULL,
  `tel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `note` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `index_reservations_on_time_from` (`time_from`),
  KEY `index_reservations_on_time_to` (`time_to`)
) ENGINE=InnoDB AUTO_INCREMENT=249 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reservations`
--

LOCK TABLES `reservations` WRITE;
/*!40000 ALTER TABLE `reservations` DISABLE KEYS */;
INSERT INTO `reservations` VALUES (153,'','','2015-02-19 14:54:02','2015-02-19 14:54:02','2015-02-19 14:55:00','2015-02-19 15:55:00',2,21,NULL,NULL),(154,'','','2015-02-19 14:54:02','2015-02-19 14:54:02','2015-02-19 14:55:00','2015-02-19 15:55:00',2,32,NULL,NULL),(155,'','','2015-02-19 14:54:14','2015-02-19 14:54:14','2015-02-19 14:55:00','2015-02-19 15:55:00',2,25,NULL,NULL),(156,'','','2015-02-19 14:54:43','2015-02-19 14:54:43','2015-02-19 14:55:00','2015-02-19 15:55:00',2,31,NULL,NULL),(157,'','','2015-02-19 14:54:43','2015-02-19 14:54:43','2015-02-19 14:55:00','2015-02-19 15:55:00',2,22,NULL,NULL),(158,'','','2015-02-19 14:54:44','2015-02-19 14:54:44','2015-02-19 14:55:00','2015-02-19 15:55:00',2,24,NULL,NULL),(159,'','','2015-02-19 14:55:13','2015-02-19 14:55:13','2015-02-19 14:55:00','2015-02-19 15:55:00',2,27,NULL,NULL),(160,'','','2015-02-19 14:55:13','2015-02-19 14:55:13','2015-02-19 14:55:00','2015-02-19 15:55:00',2,28,NULL,NULL),(161,'','','2015-02-19 14:55:13','2015-02-19 14:55:13','2015-02-19 14:55:00','2015-02-19 15:55:00',2,29,NULL,NULL),(162,'','','2015-02-19 14:55:13','2015-02-19 14:55:13','2015-02-19 14:55:00','2015-02-19 15:55:00',2,30,NULL,NULL),(163,'','','2015-02-19 14:55:13','2015-02-19 14:55:13','2015-02-19 14:55:00','2015-02-19 15:55:00',2,33,NULL,NULL),(164,'','','2015-02-19 14:55:13','2015-02-19 14:55:13','2015-02-19 14:55:00','2015-02-19 15:55:00',2,34,NULL,NULL),(165,'','','2015-02-19 14:55:13','2015-02-19 14:55:13','2015-02-19 14:55:00','2015-02-19 15:55:00',2,36,NULL,NULL),(166,'','','2015-02-19 14:55:13','2015-02-19 14:55:13','2015-02-19 14:55:00','2015-02-19 15:55:00',2,26,NULL,NULL),(167,'','','2015-02-19 14:55:14','2015-02-19 14:55:14','2015-02-19 14:55:00','2015-02-19 15:55:00',2,37,NULL,NULL),(168,'','','2015-02-19 14:55:14','2015-02-19 14:55:14','2015-02-19 14:55:00','2015-02-19 15:55:00',2,35,NULL,NULL),(169,'','','2015-02-19 14:55:14','2015-02-19 14:55:14','2015-02-19 14:55:00','2015-02-19 15:55:00',2,29,NULL,NULL),(171,'Lukáš Březina','brezina.lukas@seznam.cz','2015-02-19 16:28:17','2015-02-19 16:28:17','2015-02-19 18:30:00','2015-02-19 19:30:00',2,21,NULL,NULL),(172,'Lukáš Březina','brezina.lukas@seznam.cz','2015-02-19 17:00:23','2015-02-19 17:00:23','2015-02-19 17:05:00','2015-02-19 18:05:00',2,21,NULL,NULL),(173,'Lukáš Březina','brezina.lukas@seznam.cz','2015-02-19 18:17:22','2015-02-19 18:17:22','2015-02-19 18:20:00','2015-02-19 19:20:00',2,32,NULL,NULL),(174,'Lukáš Březina','brezina.lukas@seznam.cz','2015-02-19 18:17:38','2015-02-19 18:17:38','2015-02-19 18:30:00','2015-02-19 19:30:00',2,25,NULL,NULL),(175,'Lukáš Březina','brezina.lukas@seznam.cz','2015-02-19 18:25:34','2015-02-19 18:25:34','2015-02-19 19:00:00','2015-02-19 20:30:00',2,22,NULL,NULL),(176,'Lukáš Březina','brezina.lukas@seznam.cz','2015-02-19 18:25:53','2015-02-19 18:25:53','2015-02-19 18:45:00','2015-02-19 19:45:00',2,24,NULL,NULL),(177,'Lukáš Březina','brezina.lukas@seznam.cz','2015-02-19 18:26:25','2015-02-19 18:26:25','2015-02-19 18:30:00','2015-02-19 19:30:00',2,7,NULL,NULL),(178,'Lukáš Březina','brezina.lukas@seznam.cz','2015-02-19 18:26:39','2015-02-19 18:26:39','2015-02-19 21:30:00','2015-02-19 22:30:00',2,7,NULL,NULL),(179,'Lukáš Březina','brezina.lukas@seznam.cz','2015-02-19 18:26:50','2015-02-19 18:26:50','2015-02-19 18:45:00','2015-02-19 19:45:00',2,9,NULL,NULL),(180,'Lukáš Březina','brezina.lukas@seznam.cz','2015-02-19 18:27:02','2015-02-19 18:27:02','2015-02-19 18:50:00','2015-02-19 19:50:00',2,10,NULL,NULL),(181,'Lukáš Březina','brezina.lukas@seznam.cz','2015-02-19 20:52:04','2015-02-19 20:52:04','2015-02-19 20:55:00','2015-02-19 21:55:00',2,21,NULL,NULL),(182,'Lukáš Březina','brezina.lukas@seznam.cz','2015-02-19 20:52:23','2015-02-19 20:52:23','2015-02-19 21:20:00','2015-02-19 22:55:00',2,32,NULL,NULL),(183,'Lukáš Březina','brezina.lukas@seznam.cz','2015-02-19 20:52:42','2015-02-19 20:52:42','2015-02-19 21:55:00','2015-02-19 22:55:00',2,25,NULL,NULL),(184,'Lukáš Březina','brezina.lukas@seznam.cz','2015-02-19 20:56:28','2015-02-19 20:56:28','2015-02-19 22:00:00','2015-02-19 23:00:00',2,21,NULL,NULL),(185,'Lukáš Březina','brezina.lukas@seznam.cz','2015-02-20 10:41:59','2015-02-20 10:41:59','2015-02-20 12:45:00','2015-02-20 13:45:00',2,7,NULL,NULL),(186,'Lukáš Březina','brezina.lukas@seznam.cz','2015-02-20 10:42:36','2015-02-20 10:42:36','2015-02-20 10:55:00','2015-02-20 11:55:00',2,21,NULL,NULL),(187,'Lukáš Březina','brezina.lukas@seznam.cz','2015-02-20 10:42:36','2015-02-20 10:42:36','2015-02-20 10:55:00','2015-02-20 11:55:00',2,7,NULL,NULL),(188,'Lukáš Březina','brezina.lukas@seznam.cz','2015-02-20 10:57:27','2015-02-20 10:57:27','2015-02-20 11:00:00','2015-02-20 12:00:00',2,9,NULL,NULL),(189,'Lukáš Březina','brezina.lukas@seznam.cz','2015-02-20 11:04:02','2015-02-20 11:04:02','2015-02-20 14:05:00','2015-02-20 15:05:00',2,10,NULL,NULL),(190,'Lukáš Březina','brezina.lukas@seznam.cz','2015-02-20 11:04:02','2015-02-20 11:04:02','2015-02-20 14:05:00','2015-02-20 15:05:00',2,11,NULL,NULL),(191,'Lukáš Březina','brezina.lukas@seznam.cz','2015-02-20 11:04:03','2015-02-20 11:04:03','2015-02-20 14:05:00','2015-02-20 15:05:00',2,12,NULL,NULL),(192,'Lukáš Březina','brezina.lukas@seznam.cz','2015-02-20 11:04:03','2015-02-20 11:04:03','2015-02-20 14:05:00','2015-02-20 15:05:00',2,14,NULL,NULL),(193,'Lukáš Březina','brezina.lukas@seznam.cz','2015-02-20 11:04:03','2015-02-20 11:04:03','2015-02-20 14:05:00','2015-02-20 15:05:00',2,16,NULL,NULL),(194,'Lukáš Březina','brezina.lukas@seznam.cz','2015-02-20 11:04:03','2015-02-20 11:04:03','2015-02-20 14:05:00','2015-02-20 15:05:00',2,20,NULL,NULL),(195,'Lukáš Březina','brezina.lukas@seznam.cz','2015-02-20 13:11:37','2015-02-20 13:11:37','2015-02-20 13:15:00','2015-02-20 14:15:00',2,9,NULL,NULL),(196,'Lukáš Březina','brezina.lukas@seznam.cz','2015-02-20 13:13:35','2015-02-20 13:13:35','2015-02-20 13:15:00','2015-02-20 14:15:00',2,8,NULL,NULL),(197,'Lukáš Březina','brezina.lukas@seznam.cz','2015-02-20 20:13:20','2015-02-20 20:13:20','2015-02-20 20:15:00','2015-02-20 21:15:00',2,7,NULL,NULL),(198,'Lukáš Březina','brezina.lukas@seznam.cz','2015-02-20 20:13:20','2015-02-20 20:13:20','2015-02-20 20:15:00','2015-02-20 21:15:00',2,9,NULL,NULL),(199,'Lukáš Březina','brezina.lukas@seznam.cz','2015-02-20 20:13:20','2015-02-20 20:13:20','2015-02-20 20:15:00','2015-02-20 21:15:00',2,10,NULL,NULL),(200,'Lukáš Březina','brezina.lukas@seznam.cz','2015-02-20 20:13:20','2015-02-20 20:13:20','2015-02-20 20:15:00','2015-02-20 21:15:00',2,11,NULL,NULL),(201,'Lukáš Březina','brezina.lukas@seznam.cz','2015-02-20 21:34:49','2015-02-20 21:34:49','2015-02-20 22:00:00','2015-02-20 23:00:00',2,21,NULL,NULL),(202,'Lukáš Březina','brezina.lukas@seznam.cz','2015-02-20 21:34:50','2015-02-20 21:34:50','2015-02-20 22:00:00','2015-02-20 23:00:00',2,32,NULL,NULL),(203,'Lukáš Březina','brezina.lukas@seznam.cz','2015-02-20 21:34:50','2015-02-20 21:34:50','2015-02-20 22:00:00','2015-02-20 23:00:00',2,25,NULL,NULL),(204,'Lukáš Březina','brezina.lukas@seznam.cz','2015-02-20 21:34:50','2015-02-20 21:34:50','2015-02-20 22:00:00','2015-02-20 23:00:00',2,22,NULL,NULL),(205,'Lukáš Březina','brezina.lukas@seznam.cz','2015-02-20 21:34:50','2015-02-20 21:34:50','2015-02-20 22:00:00','2015-02-20 23:00:00',2,24,NULL,NULL),(206,'Lukáš Březina','brezina.lukas@seznam.cz','2015-02-20 21:43:53','2015-02-20 21:43:53','2015-02-20 22:00:00','2015-02-20 23:00:00',2,7,'7765667','Ahoj mami jak se máš?'),(207,'Lukáš Březina','brezina.lukas@seznam.cz','2015-02-20 21:43:53','2015-02-20 21:43:53','2015-02-20 22:00:00','2015-02-20 23:00:00',2,9,'7765667','Ahoj mami jak se máš?'),(208,'Lukáš Březina','brezina.lukas@seznam.cz','2015-02-20 21:43:53','2015-02-20 21:43:53','2015-02-20 22:00:00','2015-02-20 23:00:00',2,10,'7765667','Ahoj mami jak se máš?'),(209,'Lukáš Březina','brezina.lukas@seznam.cz','2015-02-20 21:43:53','2015-02-20 21:43:53','2015-02-20 22:00:00','2015-02-20 23:00:00',2,11,'7765667','Ahoj mami jak se máš?'),(210,'Lukáš Březina','brezina.lukas@seznam.cz','2015-02-20 21:43:53','2015-02-20 21:43:53','2015-02-20 22:00:00','2015-02-20 23:00:00',2,12,'7765667','Ahoj mami jak se máš?'),(211,'Lukáš Březina','brezina.lukas@seznam.cz','2015-02-21 08:37:08','2015-02-21 08:37:08','2015-02-21 14:00:00','2015-02-21 15:00:00',2,14,'7765667',''),(212,'Lukáš Březina','brezina.lukas@seznam.cz','2015-02-21 18:53:58','2015-02-21 18:53:58','2015-02-21 18:55:00','2015-02-21 19:55:00',2,8,'7765667','Chci dětskou židli'),(213,'Lukáš Březina','brezina.lukas@seznam.cz','2015-02-21 18:53:59','2015-02-21 18:53:59','2015-02-21 18:55:00','2015-02-21 19:55:00',2,7,'7765667','Chci dětskou židli'),(214,'Lukáš Březina','brezina.lukas@seznam.cz','2015-02-22 17:31:16','2015-02-22 17:31:16','2015-02-21 23:00:00','2015-02-22 00:00:00',2,0,'7765667','Ahoj'),(215,'Lukáš Březina','brezina.lukas@seznam.cz','2015-02-22 17:32:57','2015-02-22 17:32:57','2015-02-22 17:45:00','2015-02-22 18:45:00',2,7,'7765667','xfghn'),(216,'Luboš','shgfhfgs@adf.cz','2015-02-24 06:33:58','2015-02-24 06:33:58','2015-02-27 07:00:00','2015-02-27 08:00:00',4,15,'765555656',''),(217,'Luboš','shgfhfgs@adf.cz','2015-02-24 06:33:58','2015-02-24 06:33:58','2015-02-27 07:00:00','2015-02-27 08:00:00',4,8,'765555656',''),(218,'Lukáš Březina','brezina.lukas@seznam.cz','2015-02-24 06:34:21','2015-02-24 06:34:21','2015-02-24 07:00:00','2015-02-24 08:00:00',2,7,'7765667',''),(219,'Lukáš Březina','brezina.lukas@seznam.cz','2015-02-26 23:28:48','2015-02-26 23:28:48','2015-02-27 01:00:00','2015-02-27 02:00:00',2,7,'7765667','hjnkm'),(220,'Lukáš Březina','brezina.lukas@seznam.cz','2015-02-26 23:36:39','2015-02-26 23:36:39','2015-02-27 01:00:00','2015-02-27 02:00:00',2,9,'7765667',''),(221,'Lukáš Březina','brezina.lukas@seznam.cz','2015-02-26 23:38:00','2015-02-26 23:38:00','2015-02-27 01:00:00','2015-02-27 02:00:00',2,10,'7765667',''),(222,'Lukáš Březina','brezina.lukas@seznam.cz','2015-02-26 23:41:33','2015-02-26 23:41:33','2015-02-27 01:00:00','2015-02-27 02:00:00',2,11,'7765667',''),(223,'Lukáš Březina','brezina.lukas@seznam.cz','2015-02-26 23:43:44','2015-02-26 23:43:44','2015-02-27 01:00:00','2015-02-27 02:00:00',2,12,'7765667',''),(224,'Lukáš Březina','brezina.lukas@seznam.cz','2015-02-26 23:43:44','2015-02-26 23:43:44','2015-02-27 01:00:00','2015-02-27 02:00:00',2,12,'7765667',''),(225,'Lukáš Březina','brezina.lukas@seznam.cz','2015-02-26 23:45:01','2015-02-26 23:45:01','2015-02-27 01:00:00','2015-02-27 02:00:00',2,14,'7765667','j'),(226,'Lukáš Březina','brezina.lukas@seznam.cz','2015-02-26 23:45:02','2015-02-26 23:45:02','2015-02-27 01:00:00','2015-02-27 02:00:00',2,14,'7765667','j'),(227,'Lukáš Březina','brezina.lukas@seznam.cz','2015-02-26 23:45:05','2015-02-26 23:45:05','2015-02-27 01:00:00','2015-02-27 02:00:00',2,14,'7765667','j'),(228,'Lukáš Březina','brezina.lukas@seznam.cz','2015-02-26 23:45:06','2015-02-26 23:45:06','2015-02-27 01:00:00','2015-02-27 02:00:00',2,14,'7765667','j'),(229,'Lukáš Březina','brezina.lukas@seznam.cz','2015-02-26 23:45:06','2015-02-26 23:45:06','2015-02-27 01:00:00','2015-02-27 02:00:00',2,14,'7765667','j'),(230,'Lukáš Březina','brezina.lukas@seznam.cz','2015-02-26 23:45:06','2015-02-26 23:45:06','2015-02-27 01:00:00','2015-02-27 02:00:00',2,14,'7765667','j'),(231,'Lukáš Březina','brezina.lukas@seznam.cz','2015-02-27 00:19:36','2015-02-27 00:19:36','2015-02-27 01:00:00','2015-02-27 02:00:00',2,16,'7765667',''),(232,'Lukáš Březina','brezina.lukas@seznam.cz','2015-02-27 00:22:13','2015-02-27 00:22:13','2015-02-27 01:00:00','2015-02-27 02:00:00',2,20,'7765667',''),(233,'Lukáš Březina','brezina.lukas@seznam.cz','2015-02-27 00:29:22','2015-02-27 00:29:22','2015-02-27 01:00:00','2015-02-27 02:00:00',2,8,'7765667',''),(234,'adsf','fds@gf.ds','2015-02-27 00:38:09','2015-02-27 00:38:09','2015-02-27 01:00:00','2015-02-27 02:00:00',2,13,'2345623',''),(235,'adsf','fds@gf.ds','2015-02-27 00:40:11','2015-02-27 00:40:11','2015-02-27 01:00:00','2015-02-27 02:00:00',2,15,'2345623',''),(236,'Lukáš Březina','brezina.lukas@seznam.cz','2015-02-27 00:40:58','2015-02-27 00:40:58','2015-02-27 01:00:00','2015-02-27 02:00:00',2,17,'7765667',''),(237,'Lukáš Březina','brezina.lukas@seznam.cz','2015-02-27 00:44:30','2015-02-27 00:44:30','2015-02-27 01:00:00','2015-02-27 02:00:00',2,18,'7765667',''),(238,'Lukáš Březina','brezina.lukas@seznam.cz','2015-02-27 00:45:28','2015-02-27 00:45:28','2015-02-27 01:00:00','2015-02-27 02:00:00',2,19,'7765667','nm'),(239,'Lukáš Březina','brezina.lukas@seznam.cz','2015-02-27 00:47:01','2015-02-27 00:47:01','2015-02-27 01:00:00','2015-02-27 02:00:00',2,21,'7765667','vcxy'),(240,'Lukáš Březina','brezina.lukas@seznam.cz','2015-02-27 06:59:22','2015-02-27 06:59:22','2015-02-27 07:00:00','2015-02-27 08:00:00',2,7,'7765667',''),(241,'Lukáš Březina','brezina.lukas@seznam.cz','2015-02-27 08:56:48','2015-02-27 08:56:48','2015-02-27 13:00:00','2015-02-27 14:00:00',2,7,'7765667','xcvbnm,'),(242,'Lukáš Březina','brezina.lukas@seznam.cz','2015-02-28 12:18:15','2015-02-28 12:18:15','2015-02-28 17:00:00','2015-02-28 21:00:00',4,8,'7765667','Bylo by možno přidat dětskou stoličku? Děkuji.'),(243,'Lukáš Březina','brezina.lukas@seznam.cz','2015-02-28 12:18:15','2015-02-28 12:18:15','2015-02-28 17:00:00','2015-02-28 21:00:00',4,34,'7765667','Bylo by možno přidat dětskou stoličku? Děkuji.'),(244,'Karel Maňák','emal@example.com','2015-02-28 12:19:15','2015-02-28 12:19:15','2015-02-28 18:00:00','2015-02-28 19:00:00',2,7,'76543210',''),(245,'Klára Březinová','brezina.lukas@seznam.cz','2015-02-28 12:20:16','2015-02-28 12:20:16','2015-02-28 12:25:00','2015-02-28 13:25:00',2,7,'7765667',''),(246,'Leona Nováková','emal@example.com','2015-02-28 12:20:45','2015-02-28 12:20:45','2015-02-28 12:45:00','2015-02-28 13:45:00',2,9,'7765667',''),(247,'Lukáš Březina','brezina.lukas@seznam.cz','2015-03-06 17:01:51','2015-03-06 17:01:51','2015-03-06 17:05:00','2015-03-06 18:05:00',2,7,'7765667','');
/*!40000 ALTER TABLE `reservations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `restaurant_opening_exceptions`
--

DROP TABLE IF EXISTS `restaurant_opening_exceptions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `restaurant_opening_exceptions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date_when` date DEFAULT NULL,
  `opening` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `restaurant_opening_exceptions`
--

LOCK TABLES `restaurant_opening_exceptions` WRITE;
/*!40000 ALTER TABLE `restaurant_opening_exceptions` DISABLE KEYS */;
INSERT INTO `restaurant_opening_exceptions` VALUES (11,'2015-02-28','--- !ruby/hash:ActionController::Parameters\nfrom: !ruby/hash:ActiveSupport::HashWithIndifferentAccess\n  h: \'13\'\n  m: \'00\'\nto: !ruby/hash:ActiveSupport::HashWithIndifferentAccess\n  h: \'22\'\n  m: \'00\'\n','2015-02-05 06:49:42','2015-02-28 12:19:41'),(12,'2015-02-26','--- !ruby/hash:ActionController::Parameters\nfrom: !ruby/hash:ActiveSupport::HashWithIndifferentAccess\n  h: \'00\'\n  m: \'00\'\nto: !ruby/hash:ActiveSupport::HashWithIndifferentAccess\n  h: \'00\'\n  m: \'00\'\nclosed: \'on\'\n','2015-02-05 06:50:21','2015-02-05 06:50:21'),(13,'2015-03-28','--- !ruby/hash:ActionController::Parameters\nfrom: !ruby/hash:ActiveSupport::HashWithIndifferentAccess\n  h: \'16\'\n  m: \'00\'\nto: !ruby/hash:ActiveSupport::HashWithIndifferentAccess\n  h: \'24\'\n  m: \'00\'\n','2015-02-05 06:57:40','2015-02-05 06:58:06'),(14,'2015-02-21','--- !ruby/hash:ActionController::Parameters\nfrom: !ruby/hash:ActiveSupport::HashWithIndifferentAccess\n  h: \'15\'\n  m: \'00\'\nto: !ruby/hash:ActiveSupport::HashWithIndifferentAccess\n  h: \'24\'\n  m: \'00\'\n','2015-02-05 06:58:20','2015-02-05 06:58:20'),(17,'2015-02-08','--- !ruby/hash:ActionController::Parameters\nfrom: !ruby/hash:ActiveSupport::HashWithIndifferentAccess\n  h: \'00\'\n  m: \'00\'\nto: !ruby/hash:ActiveSupport::HashWithIndifferentAccess\n  h: \'21\'\n  m: \'00\'\n','2015-02-08 19:20:24','2015-02-08 19:27:29'),(18,'2015-02-19','--- !ruby/hash:ActionController::Parameters\nfrom: !ruby/hash:ActiveSupport::HashWithIndifferentAccess\n  h: \'02\'\n  m: \'00\'\nto: !ruby/hash:ActiveSupport::HashWithIndifferentAccess\n  h: \'24\'\n  m: \'00\'\n','2015-02-19 18:25:09','2015-02-19 18:25:09'),(19,'2015-02-20','--- !ruby/hash:ActionController::Parameters\nfrom: !ruby/hash:ActiveSupport::HashWithIndifferentAccess\n  h: \'00\'\n  m: \'00\'\nto: !ruby/hash:ActiveSupport::HashWithIndifferentAccess\n  h: \'24\'\n  m: \'00\'\n','2015-02-20 21:34:13','2015-02-20 21:34:13'),(21,'2015-03-12','--- !ruby/hash:ActionController::Parameters\nfrom: !ruby/hash:ActiveSupport::HashWithIndifferentAccess\n  h: \'15\'\n  m: \'00\'\nto: !ruby/hash:ActiveSupport::HashWithIndifferentAccess\n  h: \'24\'\n  m: \'00\'\nclosed: \'on\'\n','2015-02-28 12:23:51','2015-02-28 12:23:51');
/*!40000 ALTER TABLE `restaurant_opening_exceptions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `restaurant_settings`
--

DROP TABLE IF EXISTS `restaurant_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `restaurant_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `restaurant_scheme_file_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `restaurant_scheme_content_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `restaurant_scheme_file_size` int(11) DEFAULT NULL,
  `restaurant_scheme_updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `opening_hours` text COLLATE utf8_unicode_ci,
  `general_settings` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `restaurant_settings`
--

LOCK TABLES `restaurant_settings` WRITE;
/*!40000 ALTER TABLE `restaurant_settings` DISABLE KEYS */;
INSERT INTO `restaurant_settings` VALUES (1,NULL,NULL,NULL,NULL,NULL,'2015-01-28 00:55:15','2015-03-08 18:11:34','--- !ruby/hash:ActionController::Parameters\nmon: !ruby/hash:ActiveSupport::HashWithIndifferentAccess\n  from: !ruby/hash:ActiveSupport::HashWithIndifferentAccess\n    h: 08\n    m: \'00\'\n  to: !ruby/hash:ActiveSupport::HashWithIndifferentAccess\n    h: \'20\'\n    m: \'30\'\ntue: !ruby/hash:ActiveSupport::HashWithIndifferentAccess\n  from: !ruby/hash:ActiveSupport::HashWithIndifferentAccess\n    h: 08\n    m: \'00\'\n  to: !ruby/hash:ActiveSupport::HashWithIndifferentAccess\n    h: \'20\'\n    m: \'30\'\nwed: !ruby/hash:ActiveSupport::HashWithIndifferentAccess\n  from: !ruby/hash:ActiveSupport::HashWithIndifferentAccess\n    h: 08\n    m: \'00\'\n  to: !ruby/hash:ActiveSupport::HashWithIndifferentAccess\n    h: \'20\'\n    m: \'30\'\nthu: !ruby/hash:ActiveSupport::HashWithIndifferentAccess\n  from: !ruby/hash:ActiveSupport::HashWithIndifferentAccess\n    h: 08\n    m: \'00\'\n  to: !ruby/hash:ActiveSupport::HashWithIndifferentAccess\n    h: \'20\'\n    m: \'30\'\nfri: !ruby/hash:ActiveSupport::HashWithIndifferentAccess\n  from: !ruby/hash:ActiveSupport::HashWithIndifferentAccess\n    h: \'02\'\n    m: \'00\'\n  to: !ruby/hash:ActiveSupport::HashWithIndifferentAccess\n    h: \'22\'\n    m: \'30\'\nsat: !ruby/hash:ActiveSupport::HashWithIndifferentAccess\n  from: !ruby/hash:ActiveSupport::HashWithIndifferentAccess\n    h: 08\n    m: \'00\'\n  to: !ruby/hash:ActiveSupport::HashWithIndifferentAccess\n    h: \'22\'\n    m: \'30\'\nsun: !ruby/hash:ActiveSupport::HashWithIndifferentAccess\n  from: !ruby/hash:ActiveSupport::HashWithIndifferentAccess\n    h: 08\n    m: \'00\'\n  to: !ruby/hash:ActiveSupport::HashWithIndifferentAccess\n    h: \'24\'\n    m: \'00\'\n','--- !ruby/hash:ActionController::Parameters\nrestaurant_name: Cyklobar\ncontact: !ruby/hash:ActiveSupport::HashWithIndifferentAccess\n  address: Dobrá 957, 739 51\n  facebook: http://facebook.com\n  phone: \"+420 606 234 807\"\n  email: cyklobar@cyklobar.cz\n');
/*!40000 ALTER TABLE `restaurant_settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rooms`
--

DROP TABLE IF EXISTS `rooms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rooms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `smoking` tinyint(1) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rooms`
--

LOCK TABLES `rooms` WRITE;
/*!40000 ALTER TABLE `rooms` DISABLE KEYS */;
INSERT INTO `rooms` VALUES (1,'Super místnost',1,'2015-01-04 10:57:25','2015-02-08 19:06:55'),(2,'Místnost 2',0,'2015-01-24 10:44:32','2015-02-07 21:42:03');
/*!40000 ALTER TABLE `rooms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `schema_migrations`
--

DROP TABLE IF EXISTS `schema_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `schema_migrations` (
  `version` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  UNIQUE KEY `unique_schema_migrations` (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `schema_migrations`
--

LOCK TABLES `schema_migrations` WRITE;
/*!40000 ALTER TABLE `schema_migrations` DISABLE KEYS */;
INSERT INTO `schema_migrations` VALUES ('20140928181310'),('20140929161725'),('20140929161953'),('20141002140659'),('20141003122253'),('20141025165703'),('20141025165841'),('20141025171643'),('20141025172612'),('20141025172736'),('20141026121704'),('20141026161055'),('20141026192639'),('20141120073310'),('20141120074244'),('20141120074821'),('20141121062233'),('20141121214008'),('20141121214216'),('20141123172815'),('20141123172928'),('20141123172954'),('20141210185245'),('20150101190838'),('20150101191547'),('20150103223356'),('20150115060726'),('20150124125805'),('20150126160805'),('20150129105305'),('20150206234349'),('20150212153258'),('20150216184856'),('20150219080503'),('20150219084807'),('20150220123954'),('20150224103629'),('20150305124939'),('20150306175739'),('20150306184005');
/*!40000 ALTER TABLE `schema_migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `static_pages`
--

DROP TABLE IF EXISTS `static_pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `static_pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8_unicode_ci,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `settings` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `index_static_pages_on_slug` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `static_pages`
--

LOCK TABLES `static_pages` WRITE;
/*!40000 ALTER TABLE `static_pages` DISABLE KEYS */;
INSERT INTO `static_pages` VALUES (1,'Hlavní strana','','2015-02-25 21:24:55','2015-03-06 22:24:46','hlavni-strana','--- !ruby/hash:ActionController::Parameters\nfood_carousel_title: \'<p><span style=\"font-size: 24pt;\">Přijďte si vyzkou&scaron;et\n  na&scaron;e nov&eacute; pizzy!</span></p>\'\nfood_carousel_category: \'63\'\n'),(2,'O cyklobaru','<p><span style=\"font-size: 18pt;\">V&iacute;t&aacute;me V&aacute;s v are&aacute;lu cyklobaru,</span></p>\r\n<p><strong>v Dobr&eacute; u Fr&yacute;dku-M&iacute;stku</strong>!&nbsp;Jde o kl&iacute;čov&eacute; m&iacute;sto pro přesuny nejen cyklistů a cykloturistů ve směru z&nbsp;F-M do Beskyd (na Mor&aacute;vku, Kr&aacute;snou a Lysou horu) a k&nbsp;Žermanick&eacute; a Těrlick&eacute; přehradě.</p>\r\n<p>Are&aacute;l se nach&aacute;z&iacute; na <strong>trojn&aacute;sobn&eacute; cyklostezce trasy Fr&yacute;dek-M&iacute;stek-Dobr&aacute; (Br&aacute;na Beskyd č. 6003, 6004 a 6005)</strong> a z&aacute;roveň region&aacute;ln&iacute; cyklostezce č. 502 Jihlava - Česk&yacute; Tě&scaron;&iacute;n, kter&aacute; se vine mezi hlavn&iacute; silnic&iacute; ve směru z F-M na Dobrou a tokem řeky Mor&aacute;vky.Trasa č. 502 navazuje 20 km z&aacute;padně na d&aacute;lkovou cyklotrasu č. 5 \"Moravsk&aacute; br&aacute;na\" a v t&eacute;že vzd&aacute;lenosti v&yacute;chodně pak na př&iacute;hraničn&iacute; cyklotrasu č. 46 \"Beskydsko-karpatsk&aacute; magistr&aacute;la\" a hlavn&iacute; cyklotrasu do Polska č. 56 \"Cyklistick&yacute; okruh Euroregionem Tě&scaron;&iacute;nsk&eacute; Slezsko\".</p>\r\n<p>V na&scaron;&iacute; restauraci se na V&aacute;s tě&scaron;&iacute; př&iacute;jemn&aacute; obsluha s bohat&yacute;m v&yacute;běrem j&iacute;del a n&aacute;pojů (v nab&iacute;dce i pizza, palačinky) a nav&iacute;c můžete využ&iacute;t ubytov&aacute;n&iacute; v soukrom&iacute; v podkrov&iacute; objektu. V&nbsp;cel&eacute;m are&aacute;lu je<strong> WiFi, v&nbsp;restauraci je hostům k&nbsp;dispozici PC, dětsk&yacute; koutek, bezbari&eacute;rov&yacute; př&iacute;stup a bezbari&eacute;rov&aacute; toaleta v&nbsp;př&iacute;zem&iacute;, přebalovac&iacute; pult pro děti.</strong></p>\r\n<p>Venkovn&iacute; are&aacute;l doplňuj&iacute; terasy, parkovi&scaron;tě, dětsk&eacute; hři&scaron;tě, zahrada, ohni&scaron;tě a stylov&aacute; stodola pro poř&aacute;d&aacute;n&iacute; letn&iacute;ch akc&iacute;.</p>\r\n<p>Společensk&eacute;, rodinn&eacute; i pracovn&iacute; akce, setk&aacute;n&iacute; a prezentace je možn&eacute; poř&aacute;dat jak v prostor&aacute;ch restaurace a j&iacute;delny, tak na kryt&yacute;ch venkovn&iacute;ch teras&aacute;ch a ve stylov&eacute; stodole.</p>\r\n<p><strong>Restauračn&iacute; zař&iacute;zen&iacute; Cyklobaru splnilo v květnu 2008 podm&iacute;nky pro z&iacute;sk&aacute;n&iacute; certifik&aacute;tu &bdquo;BEZ BARI&Eacute;R&ldquo;&nbsp;</strong> v r&aacute;mci projektu Beskydy pro v&scaron;echny.V&iacute;ce informac&iacute; na&nbsp;<a href=\"http://www.jedemetaky.cz/\">www.jedemetaky.cz</a></p>\r\n<p><strong>Ubytovac&iacute; zař&iacute;zen&iacute; Cyklobaru je od roku 2007 držitelem certifik&aacute;tu &bdquo;Cyklist&eacute; v&iacute;t&aacute;ni&ldquo;</strong> udělovan&yacute;m Nadac&iacute; partnerstv&iacute;.</p>','2015-02-25 21:25:09','2015-03-08 18:56:39','o-cyklobaru',NULL),(3,'Jídelníček','','2015-02-25 21:25:20','2015-02-25 21:25:20','jidelnicek',NULL),(4,'Rezervace','','2015-02-25 21:25:32','2015-02-25 21:25:32','rezervace',NULL),(5,'Kontakt','<p>Cyklobar se nach&aacute;z&iacute; nedaleko hlavn&iacute; cesty ve směru z<em><strong> Fr&yacute;dku - M&iacute;stku do Dobr&eacute;</strong></em> po prav&eacute; straně v pol&iacute;ch za v&yacute;&scaron;kovou budovou V&Uacute;HŽ.&nbsp; Ve směru na Česk&yacute; Tě&scaron;&iacute;n nutno sjet ve Fr&yacute;dku-M&iacute;stku z rychlostn&iacute; komunikace R 48 směr Ba&scaron;ka, d&aacute;le kolem Intersparu (ne&nbsp; po obchvatu kolem Dobr&eacute;).&nbsp; D&aacute;le je možn&eacute; odbočit již za fr&yacute;deck&yacute;m městsk&yacute;m hřbitovem vpravo nebo až těsně před železničn&iacute;m přejezdem v Dobr&eacute; \"Na &Scaron;pici\" rovněž vpravo. V opačn&eacute;m směru z Dobr&eacute; do Fr&yacute;dku-M&iacute;stku (ve směru od Česk&eacute;ho Tě&scaron;&iacute;na nutno sjet z R 48) mus&iacute;te z&nbsp;centra Dobr&eacute; &nbsp;odbočit vlevo. Cyklobar se nach&aacute;z&iacute; na m&iacute;stn&iacute; komunikaci (zhruba 300 m od hlavn&iacute; cesty proch&aacute;zej&iacute;c&iacute; středem Dobr&eacute;), kter&aacute; je z&aacute;roveň několikan&aacute;sobnou cyklotrasou. Př&iacute;jezd k objektu je ze v&scaron;ech stran vyznačen reklamn&iacute;mi cedulemi.</p>','2015-02-25 21:27:19','2015-03-08 17:59:33','kontakt',NULL);
/*!40000 ALTER TABLE `static_pages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tables`
--

DROP TABLE IF EXISTS `tables`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tables` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `number` int(11) DEFAULT NULL,
  `persons` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `room_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_tables_on_room_id` (`room_id`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tables`
--

LOCK TABLES `tables` WRITE;
/*!40000 ALTER TABLE `tables` DISABLE KEYS */;
INSERT INTO `tables` VALUES (7,1,2,'2015-02-07 20:06:35','2015-02-07 20:06:35','2'),(8,2,4,'2015-02-07 20:06:35','2015-02-07 20:06:35','2'),(9,3,2,'2015-02-07 20:06:35','2015-02-07 20:06:35','2'),(10,4,2,'2015-02-07 20:06:35','2015-02-07 20:06:35','2'),(11,5,2,'2015-02-07 20:06:35','2015-02-07 20:06:35','2'),(12,6,2,'2015-02-07 20:06:35','2015-02-07 20:06:35','2'),(13,7,4,'2015-02-07 20:06:35','2015-02-07 20:06:35','2'),(14,8,2,'2015-02-07 20:06:35','2015-02-07 20:06:35','2'),(15,9,4,'2015-02-07 20:06:35','2015-02-07 20:06:35','2'),(16,10,2,'2015-02-07 20:06:35','2015-02-07 20:06:35','2'),(17,11,4,'2015-02-07 20:06:35','2015-02-07 20:06:35','2'),(18,12,4,'2015-02-07 20:06:35','2015-02-07 20:06:35','2'),(19,13,4,'2015-02-07 20:06:35','2015-02-07 20:06:35','2'),(20,14,2,'2015-02-07 20:06:35','2015-02-07 20:06:35','2'),(21,15,2,'2015-02-07 20:07:21','2015-02-07 20:07:21','1'),(22,16,4,'2015-02-07 20:07:21','2015-02-07 20:07:21','1'),(23,17,1,'2015-02-07 20:07:21','2015-02-07 20:07:21','1'),(24,18,4,'2015-02-07 20:07:21','2015-02-07 20:07:21','1'),(25,19,3,'2015-02-07 20:07:21','2015-02-07 20:07:21','1'),(26,20,6,'2015-02-07 20:07:21','2015-02-07 20:07:21','1'),(27,21,4,'2015-02-07 20:07:21','2015-02-07 20:07:21','1'),(28,22,4,'2015-02-07 20:07:21','2015-02-07 20:07:21','1'),(29,23,4,'2015-02-07 20:07:21','2015-02-07 20:07:21','1'),(30,24,4,'2015-02-07 20:07:21','2015-02-07 20:07:21','1'),(31,25,4,'2015-02-07 20:07:21','2015-02-07 20:07:21','1'),(32,26,2,'2015-02-07 20:07:21','2015-02-07 20:07:21','1'),(33,27,4,'2015-02-07 20:07:21','2015-02-07 20:07:21','1'),(34,28,4,'2015-02-07 20:07:21','2015-02-07 20:07:21','1'),(35,29,8,'2015-02-07 20:07:21','2015-02-07 20:07:21','1'),(36,30,4,'2015-02-07 20:07:21','2015-02-07 20:07:21','1'),(37,31,6,'2015-02-07 20:07:21','2015-02-07 20:07:21','1'),(38,1,5,'2015-02-19 20:55:19','2015-02-19 20:55:19','3'),(39,2,3,'2015-02-19 20:55:19','2015-02-19 20:55:19','3'),(40,3,4,'2015-02-19 20:55:19','2015-02-19 20:55:19','3'),(41,4,2,'2015-02-19 20:55:19','2015-02-19 20:55:19','3');
/*!40000 ALTER TABLE `tables` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-03-08 21:09:08
